#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This is the master script for the FEA/CFD Coupling program cplTherm.
    Execute from coupling directory, e.g.:
    python3 ~/Coupling/code/cplTherm/master.py 2>&1 | tee log
"""

__author__ = 'Alexander Schindler'
__copyright__ = 'Institute of Aerospace Thermodynamics, University of Stuttgart'
__credits__ = ['Alexander Schindler']
__license__ = 'GNU General Public License version 3'
__version__ = '0.9.16'
__maintainer__ = 'Alexander Schindler'
__email__ = 'alexander.schindler@itlr.uni-stuttgart.de'
__status__ = 'Development' # 'Prototype', 'Development', 'Production'

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The following codes have been coupled during the development from 2016-2021:
CalculiX 2.12, 2.16, 2.17, free software, licensed under GNU GPLv2, <http://www.dhondt.de>
OpenFOAM 4 and 6, free software, licensed under GNU GPLv3, <https://openfoam.org>

Extensions to allow coupling commercial codes like ANSYS CFX 18.0
(https://www.ansys.com/products/fluids/ansys-cfx) are optional, no part of this
program, and therefore only implemented on a generalized level. The actual
adapter has to be written by the user to avoid licensing incompatibilities.
"""

## Pre Settings ###############################################################
## Import modules
import copy
from collections import defaultdict  # Allows to create nested dicts on the fly
import logging
import numpy as np
import os
import shelve # to save workspace into file
import subprocess
import sys
import time
## Import own modules
import control
from convergence import check_convergence
import convertformat
import initrun
import interpolation
import post
from readinput import read_input
import setenv
import startrun

## Set logging level
VALID_ARGS = {'--log=DEBUG', '--log=INFO', '--log=WARNING', '--log=ERROR',
              '--log=CRITICAL', '--log=debug', '--log=info', '--log=warning',
              '--log=error', '--log=critical'}
# Check if command line argument exists in set VALID_ARGS
arg = VALID_ARGS & set(sys.argv)
# If one valid CL arg is given (not empty or > 1)
if len(arg) == 1:
    # Get specified loglevel
    loglevel = sys.argv[sys.argv.index(list(arg)[0])].split('--log=')[1]
else:
    # Default
    loglevel = 'WARNING'
num_level = getattr(logging, loglevel.upper(), None) # Get numeric loglevel
if not isinstance(num_level, int):
    raise ValueError('Invalid log level: %s' % loglevel)
logging.basicConfig(level=num_level, format='%(levelname)s: %(message)s',
                    stream=sys.stdout)
logger = logging.getLogger(__name__)

## Tarball
VALID_ARGS = {'--tar'}
# Check if command line argument exists in set VALID_ARGS
arg = VALID_ARGS & set(sys.argv)
# If one valid CL arg is given (not empty or > 1)
if len(arg) == 1:
    # Archive mode (useful if many (intermediate) result files are generated)
    tarball = True
else:
    # Default
    tarball = False

## Allow multiprocessing
MULTIPROC = True

## Profiling
PROFILING = False
if PROFILING == True:
    import cProfile, pstats, io
    pr = cProfile.Profile()
    pr.enable()

## Print header
## Run information
print('######################\n###### HEADER ########\n######################')
print('Code Version: ' + __version__)
print('Starting FEA/CFD Coupling at', time.asctime(time.localtime(time.time())))
print('Hostname: ' + str(os.uname()[1]))

## Set cwd as coupling directory
cwd = os.getcwd()
print('Current working directory:', cwd)

## Options
logging.debug('Debug mode: True')
print('Logging level: ', logger.getEffectiveLevel())
print('Archive mode (tarball): ', str(tarball))

## Print issues to fix in next releases (only in DEBUG mode)
logging.debug('\n######################\n###### TODO #########'
              '\n######################')
if logger.getEffectiveLevel() == 10: # Level 10 = Debug mode
    with open('~/Coupling/code/cplTherm/thingsToFix') as f:
        print(str(f.read()))

## Create simulation code classes ##############################################
class SimCode:
    """ This is the class of the simulation code. """
    def __init__(self, codeName=None, path=None, lockfile=None):
        """ TODO: Add all possible attributes and members!"""
        self.converged = []
        self.convergence_limit = []
        self.convergence_norm = []
        self.h_coeff = []
        self.input = []
        self.input_old = []
        self.input_variable = []
        self.lockfile = lockfile if lockfile is not None else []
        self.name = codeName if codeName is not None else []
        self.norm = []
        self.norm_inf = []
        self.norm_max_index = []
        self.path = path if path is not None else []
        self.runtime = {'startLoop': 0, 'totalLoop': 0}
        self.source_path = []
        # Containers for storage and easy access.
        self.dict_output = defaultdict(dict)
        self.dict_res = defaultdict(dict) # residual
        self.dict_norm = defaultdict(dict) # residual
        self.dict_output_relaxed = defaultdict(dict) # relaxed output variables
        self.dict_relax_omega_vector = defaultdict(dict) # relaxed output variables
        self.dict_approximated_order_of_convergence_mean = defaultdict(dict) # used to determine asymptotic error constant (mean value over all points)
        self.dict_asymptotic_error_constant_mean = defaultdict(dict) # mu (mean value over all points)

    def check_exist(self, checklist):
        """ This method checks if specified self.path exists. """
        for check in checklist:
            if os.path.exists(getattr(self, check)):
                print(self.name + ': ' + check + ': ' + getattr(self, check))
            else:
                sys.exit('###\nError: ' + check + ' does not exist! \n###')

    def source_code(self):
        """ This method sources the code or sets the correct environment. """
        setenv.source_code(self.name)
        return None

    def clean_casedir(self, cleanfile):
        """ This method cleans the case folders using the clean.sh scripts. """
        if os.path.exists(self.path + '/' + cleanfile):
            try:
                # Change to directory where script is executed
                os.chdir(self.path)
                # Execute bash script
                cmd = '/bin/bash -c "' + self.path + '/' + cleanfile + '"'
                print(self.name + ': Clean dir ' + os.getcwd() + ': ' + cmd)
                subprocess.check_call(cmd, shell=True)
                # Change back to current working directory
                os.chdir(cwd)
            except:
                sys.exit('###\nError: Cannot clean ' + self.name + '\n###')
        else:
            sys.exit('###\nError: Clean file ' + self.path + '/' + cleanfile
                     + ' does not exist.\n###')

    def init_run(self):
        """ This method initializes the FEA/CFD variables and files. """
        initrun.initialize(self)

    def post_run(self):
        """ This method performs post processing tasks on the fly (rename
        files etc.) after the convergence check. """
        post.post_run(self, cpl, tarball)

    def start_code(self):
        """ This method prepares the start files and runs the code. """
        print('\nStart code', self.name)
        self.runtime['startLoop'] = time.time()
        print('TODO: Eventuell starttime und runtime uebergeben.')
#         import startrun
        startrun.start(self, cpl)
        print(self.name + ': runtime =', time.time()
              - self.runtime['startLoop'], 's.')
        self.runtime['totalLoop'] += time.time() - self.runtime['startLoop']
        return self

    def convert_format_to_uni(self, read_mesh, variable):
        """ This method converts the code specific to a universal format. """
        print('\n' + self.name+ ': Convert format from code specific to '
              'universal')
        convertformat.convert_format(self, cpl, read_mesh, variable)

    def write_format(self, init_field, variable):
        """ This method writes results in the code specific format. """
        print('\nWrite input in code specific format for', self.name)
        convertformat.write_format(self, init_field, variable)

## Create coupling class #######################################################
class CplCode:
    """ This is the coupling class. """
    def __init__(self):
        self.backup = []
        self.converged = []
        self.corrector_step = []
        self.final_corrector_step = []
        self.interface_surface = []
        self.max_iter = []
        self.mode = []
        self.name = 'Coupling'
        self.n_iter = []
        self.n_outer_correctors = []
        self.path = []
        self.reset_Aitken = False
        # Dictionary for runtime information
        self.runtime = {'startMaster': time.time(), 'startCpl': 0,
                        'startCplLoop': 0, 'startInterp': 0, 'totalInterp': 0,
                        'startMeshConversion': 0}
        self.simtime = {'currentSimTime': 0.0, 'maxSimTime': 0.0, 'breakpoints': [0.0]}
        self.user_converged = True
    clean_casedir = SimCode.clean_casedir
    check_exist = SimCode.check_exist

    def interpolate_values(self, source, target, variable):
        """ This method interpolates values from source point cloud to
        target point cloud.
        """
        interpolation.interpolate_values(source, target, variable, cpl, tarball)

    def set_time(self, position, code):
        """ This method sets time steps and modifies these during runtime. """
        control.set_time(position, self, code, code.neighbor_code, tarball)

## Start Preprocessing: Prepare coupling ######################################
print('\n######################\n## Preprocessing: Prepare coupling'
      '\n######################')

## Create instances of class SimCode
logging.info('Create instances fea and cfd of class SimCode and cpl of class '
             'CplCode.')
cpl = CplCode()
fea = SimCode()
cfd = SimCode()

## Link neighbor code
fea.neighbor_code = cfd
cfd.neighbor_code = fea

## Read user run settings
read_input(input_path=cwd + '/inputDeck.inp', fea=fea, cfd=cfd,
                           cpl=cpl)

# Set cpl.path to current working directory
cpl.path = cwd
# Assume that fea folder is within cpl folder. There is only a relative path
# specified in inputDeck.inp.
fea.path = cpl.path + '/' + fea.path
cfd.path = cpl.path + '/' + cfd.path

## Check __dict__ of instances
logging.debug('cpl.__dict__: ' + str(cpl.__dict__))
logging.info('fea.__dict__: ' + str(fea.__dict__))
logging.info('cfd.__dict__: ' + str(cfd.__dict__))

## Check and source code
cfd.check_exist(['source_path', 'path'])
cfd.source_code()
fea.check_exist(['source_path', 'path'])
fea.source_code()

## Clean case directories
fea.clean_casedir(cleanfile='clean.sh')
cfd.clean_casedir(cleanfile='clean.sh')
cpl.clean_casedir(cleanfile='clean.sh')

## Print h for Robin Condition
if fea.input_variable == 'Robin' or cfd.input_variable == 'Robin':
    print('fea.hCoeff = ' + str(fea.h_coeff) + ', cfd.hCoeff = '
          + str(cfd.h_coeff))

## Initialize FEA/CFD simulation
cpl.n_iter = 1
cpl.corrector_step = 0 # Important for transient ccx boundary
cpl.set_time('pre',cfd)
cfd.init_run()
cpl.set_time('pre',fea)
fea.init_run()
fea.cpl_interval_init = fea.cpl_interval # store original fea coupling interval size

## Import user function as module
if os.path.exists(cpl.path + '/userFunctions.py'):
    print('Found userFunction.py in coupling case directory. Import module.')
    #TODO: path.append might cause problems, when there are multiple modules
    #with the name userFunctions
    try:
        sys.path.append(str(cpl.path))
        from userFunctions import userf
    except:
        sys.exit('Error: Something went wrong with the user function import.')
else:
    print('No userFunction.py in coupling case directory. Continue without.')

## Start FEA/CFD Coupling Procedure ###########################################
print('\n######################\n## Start FEA/CFD Coupling Procedure'
      '\n######################')
cpl.converged = False
logging.info('cpl.__dict__' + str(cpl.__dict__))

cpl.runtime['startCpl'] = time.time()
while (cpl.n_iter <= cpl.max_iter) and (cpl.simtime['currentSimTime'] < cpl.simtime['maxSimTime']):
    print('\n######################\n## Coupling Interval Number: '
          + str(cpl.n_iter) + '\n######################')
    cpl.runtime['startCplLoop'] = time.time()
    print('Current simulation time at start of coupling interval:', str(cpl.simtime['currentSimTime']),'s')

    if cpl.mode == 'sequential':
        # Outer loop to ensure convergence in each timestep
        cpl.corrector_step = 1
        cpl.final_corrector_step = False
        while(cpl.corrector_step <= cpl.n_outer_correctors
              and cpl.final_corrector_step == False):
            # Boolean switch cpl.final_corrector_step is set at the end of this
            # corrector_step
            print('\n######################\n## Coupling Corrector Step: '
                  + str(cpl.corrector_step) + ' (max '
                  + str(cpl.n_outer_correctors) + ')'
                  + ' [of Coupling Interval Number: ' + str(cpl.n_iter) +
                  ']\n######################')

            # Execute user function
            if os.path.exists(cpl.path + '/userFunctions.py'):
                userf('start_corrector_step', cpl, fea, cfd)
            else:
                pass

            # Run FEA #########################################################
            cpl.set_time('start_corrector_step',fea)
            
            if cpl.n_iter > 1 or cpl.corrector_step > 1:
                # Do not execute at first corr of first loop (already initialized)
                fea.write_format(init_field=False, variable=fea.input_variable)
            fea.start_code()

            # Interpolation from FEA to CFD
            cpl.runtime['startInterp'] = time.time()
            fea.convert_format_to_uni(read_mesh=False, variable=cfd.input_variable)

            # Save results to dict for postprocessing
            # Dummy write output for initial output (nu-1)
            if cpl.corrector_step == 1: # and cpl.n_iter == 1 necessary?
                if cpl.n_iter == 1:
                    fea.dict_output[cpl.n_iter][0] = copy.deepcopy(fea.output)
                    fea.dict_output[cpl.n_iter][0][:, 3].fill(0)
                else: #cpl.n_iter > 1:
                    nu_final_previous = list(fea.dict_output[cpl.n_iter-1].keys())[-1] # last subiteration of previous cplInterval
                    fea.dict_output[cpl.n_iter][0] = copy.deepcopy(fea.dict_output[cpl.n_iter-1][nu_final_previous])
            # Save FEA output to dict (for later reference)
            fea.dict_output[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(fea.output)

            # Calculate convergence of code 1 (not used for evaluation!)
            old = fea.dict_output[cpl.n_iter][cpl.corrector_step-1][:, 3]
            new = fea.dict_output[cpl.n_iter][cpl.corrector_step][:, 3]
            fea.converged, fea.norm, fea.norm_max_index, residual_current = check_convergence(
                    old, new, fea.convergence_limit, str(fea.convergence_norm))

            # Copy current residual from check_convergence() to dict
            fea.dict_res[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(residual_current)
            fea.dict_norm[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(fea.norm)
            

            
            cpl.interpolate_values(source=fea, target=cfd,
                                   variable=cfd.input_variable)

            cpl.set_time('start_corrector_step',cfd)

            cfd.write_format(init_field=False, variable=cfd.input_variable)
            print('Interpolation time =',
                  time.time()- cpl.runtime['startInterp'], 's.')
            cpl.runtime['totalInterp'] += time.time()
            - cpl.runtime['startInterp']

            # Run CFD #########################################################
            cfd.start_code()

            # Conversion to universal format
            cpl.runtime['startInterp'] = time.time()
            cfd.convert_format_to_uni(read_mesh=False,
                                      variable=fea.input_variable)
            
            ## New convergence check (on mesh 2 only, before relaxation) ######
            # Dummy write output for initial output (nu-1)
            if cpl.corrector_step == 1: # and cpl.n_iter == 1 necessary?
                if cpl.n_iter == 1:
                    cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step-1] = copy.deepcopy(cfd.output) # must work for == 1 as well, because relaxation won't work otherwise.
                    # TODO: Check if equality to next is a problem or if this could be the same as if starting from homogeneous with relax = 1?
                    cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step-1][:, 3].fill(0) # Init with zeroes (either T=0 or q=0 or Tref=0)
                else: #cpl.n_iter > 1:
                    nu_final_previous = list(cfd.dict_output_relaxed[cpl.n_iter-1].keys())[-1] # last subiteration of previous cplInterval
                    cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step-1] = copy.deepcopy(cfd.dict_output_relaxed[cpl.n_iter-1][nu_final_previous])
            
            # Save CFD output to dict (for relaxation and later reference)
            cfd.dict_output[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(cfd.output)
            
            # if fea.input_variable == 'heatFlux' or fea.input_variable == 'temperature' or fea.input_variable == 'Robin': # equals cfd.output variable
            # is always true, as [:,3] is heatFlux or temperature or reference temperature (Robin), depending on the fea.input_variable
            old = cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step-1][:, 3]
            new = cfd.dict_output[cpl.n_iter][cpl.corrector_step][:, 3]
            cfd.converged, cfd.norm, cfd.norm_max_index, residual_current = check_convergence(
                    old, new, cfd.convergence_limit, str(cfd.convergence_norm))

            
            # Copy current residual from check_convergence() to dict
            cfd.dict_res[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(residual_current)
            cfd.dict_norm[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(cfd.norm)
                    
            # Allocate correct size (and x,y,z) for current relaxed output and modify column afterwards; (cpl.corrector_step-1) already exists)
            cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step] = copy.deepcopy(cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step-1]) 
            
            # Distinguish between relaxation methods how omega is calculated
            if cfd.relax_method == 'none':
                cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.full_like(cfd.dict_res[cpl.n_iter][cpl.corrector_step], 1) # array size of residual but with ones values filled in
            elif cfd.relax_method == 'constant':
#                if fea.input_variable == 'heatFlux' or fea.input_variable == 'temperature' or fea.input_variable == 'Robin': # equals cfd.output variable
                    # [:,3] is heatFlux or temperature or reference temperature (Robin)
                cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.full_like(cfd.dict_res[cpl.n_iter][cpl.corrector_step], cfd.relax_omega_init) # array size of residual but with omega values filled in
            elif cfd.relax_method == 'Aitken':
                if cpl.corrector_step == 1:
                    # This means that at each new coupling interval (cpl.n_iter) omega is reset to initialization for first corrector.
                    cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.full_like(cfd.dict_res[cpl.n_iter][cpl.corrector_step], cfd.relax_omega_init) # array size of residual but with omega values filled in
                else: # cpl.corrector_step > 1:
                    # Avoid zero denominator
                    if np.linalg.norm(cfd.dict_res[cpl.n_iter][cpl.corrector_step] - cfd.dict_res[cpl.n_iter][cpl.corrector_step-1], np.inf) > cfd.convergence_limit:
                        # Calculate new omega based on previous subiteration.
                        relax_omega_dimensions = 'vector' # Distinguish between locally constant or locally varying omega
#                        if cpl.corrector_step <= 2:
#                            relax_omega_dimensions = 'scalar_based_on_2ndIter' # Distinguish between locally constant or locally varying omega
#                        elif cpl.corrector_step > 2:
#                            relax_omega_dimensions = 'vector' # Distinguish between locally constant or locally varying omega
#                        relax_omega_dimensions = 'scalar_based_on_2ndIter' # Distinguish between locally constant or locally varying omega
#                        relax_omega_dimensions = 'scalar_based_on_2ndIter_scaled' # Distinguish between locally constant or locally varying omega
#                        relax_omega_dimensions = 'scalar_abs' # Distinguish between locally constant or locally varying omega

                        if relax_omega_dimensions == 'scalar':
                            cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.multiply(
                                    -cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step-1], 
                                    np.divide(cfd.dict_res[cpl.n_iter][cpl.corrector_step-1], 
                                              (cfd.dict_res[cpl.n_iter][cpl.corrector_step]-cfd.dict_res[cpl.n_iter][cpl.corrector_step-1])
                                              )
                                    )  # array size of residual but with omega values filled in
                        elif relax_omega_dimensions == 'scalar_abs': # try & error
                            cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.multiply(
                                    cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step-1], 
                                    np.divide(np.abs(cfd.dict_res[cpl.n_iter][cpl.corrector_step-1]), 
                                              np.abs(cfd.dict_res[cpl.n_iter][cpl.corrector_step]-cfd.dict_res[cpl.n_iter][cpl.corrector_step-1])
                                              )
                                    )  # array size of residual but with omega values filled in
                        elif relax_omega_dimensions == 'scalar_based_on_2ndIter':
                            cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.multiply(
                                    -cfd.dict_relax_omega_vector[cpl.n_iter][2-1], 
                                    np.divide(cfd.dict_res[cpl.n_iter][2-1], 
                                              (cfd.dict_res[cpl.n_iter][2]-cfd.dict_res[cpl.n_iter][2-1])
                                              )
                                    )  # array size of residual but with omega values filled in
                        elif relax_omega_dimensions == 'scalar_based_on_2ndIter_scaled':
                            cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.multiply(
                                    np.multiply( # scalar_based_on_2ndIter
#                                    -cfd.dict_relax_omega_vector[cpl.n_iter][2-1],
                                    -1,
                                    np.divide(cfd.dict_res[cpl.n_iter][2-1], 
                                              (cfd.dict_res[cpl.n_iter][2]-cfd.dict_res[cpl.n_iter][2-1])
                                              )
                                    ),
                                    np.multiply(
                                    -cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step-1], 
                                    np.divide(np.dot(cfd.dict_res[cpl.n_iter][cpl.corrector_step-1],(cfd.dict_res[cpl.n_iter][cpl.corrector_step]-cfd.dict_res[cpl.n_iter][cpl.corrector_step-1])), 
                                              np.linalg.norm(cfd.dict_res[cpl.n_iter][cpl.corrector_step]-cfd.dict_res[cpl.n_iter][cpl.corrector_step-1],2)**2
                                              )
                                              )
                                    )  # array size of residual but with omega values filled in
#                            cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step][cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step]<0.01] = 0.01
#                            cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step][cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step]>1] = 1
                        elif relax_omega_dimensions == 'vector':
#                            if (cpl.n_iter == 2) and (cpl.corrector_step == 7):
#                                print('Wait')
                            if (cpl.reset_Aitken == True): # After dynamic coupling interval size adjustment
                                # Treat like "if cpl.corrector_step == 1:"
                                cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.full_like(cfd.dict_res[cpl.n_iter][cpl.corrector_step], cfd.relax_omega_init) # array size of residual but with omega values filled in
                                cpl.reset_Aitken = False # Reset to normal mode again
                            else:
                                cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = np.multiply(
                                        -cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step-1], 
                                        np.divide(np.dot(cfd.dict_res[cpl.n_iter][cpl.corrector_step-1],(cfd.dict_res[cpl.n_iter][cpl.corrector_step]-cfd.dict_res[cpl.n_iter][cpl.corrector_step-1])), 
                                                  np.linalg.norm(cfd.dict_res[cpl.n_iter][cpl.corrector_step]-cfd.dict_res[cpl.n_iter][cpl.corrector_step-1],2)**2
                                                  )
                                        )  # array size of residual but with omega values filled in
                        else:
                            sys.exit('Choose the relaxation parameter to be either scalar or vector.')
                    else:
                        logging.warning('Aitken relaxation: denominator below threshold, omega not updated.')
                        cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step] = cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step-1] # do nothing, keep old relax_omega_vector
            else:
                sys.exit('Incorrect relax_method set.')

            cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step][:, 3] = np.add(cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step-1][:, 3],
                                   np.multiply(cfd.dict_relax_omega_vector[cpl.n_iter][cpl.corrector_step], cfd.dict_res[cpl.n_iter][cpl.corrector_step]))

            # Overwrite cfd.output with relaxed value, so that the relaxed value is used for interpolation and so on.
            cfd.output = copy.deepcopy(cfd.dict_output_relaxed[cpl.n_iter][cpl.corrector_step])

            
            # Interpolation from CFD to FEA
            cpl.interpolate_values(source=cfd, target=fea,
                                   variable=fea.input_variable)
            print('Interpolation time =',
                  time.time() - cpl.runtime['startInterp'], 's.')
            cpl.runtime['totalInterp'] += time.time()
            - cpl.runtime['startInterp']


            # Print convergence info
            logging.info('[cfd.converged, cfd.norm] = '
                         + str([cfd.converged, cfd.norm])
                         + ' at Coupling Corrector Step: '
                         + str(cpl.corrector_step) + ' (max '
                         + str(cpl.n_outer_correctors) + ')'
                         + ' [of Coupling Interval Number: '
                         + str(cpl.n_iter) + ']')

            ### Calculate rate of convergence, Performance is bad as it is repeated from scratch each time...
            # Locally resolved
            cfd.dict_approximated_order_of_convergence = copy.deepcopy(cfd.dict_res)
            cfd.dict_asymptotic_error_constant = copy.deepcopy(cfd.dict_res)
            for n_iter in cfd.dict_res:
                for corrector_step in cfd.dict_res[n_iter]:
                    if corrector_step < 3:
                        cfd.dict_approximated_order_of_convergence[n_iter][corrector_step][:] = np.nan
                        cfd.dict_asymptotic_error_constant[n_iter][corrector_step][:] = np.nan
                    else: # n_iter > 2
                        cfd.dict_approximated_order_of_convergence[n_iter][corrector_step] = np.log10(np.abs((cfd.dict_res[n_iter][corrector_step])/(cfd.dict_res[n_iter][corrector_step-1])))/np.log10(np.abs((cfd.dict_res[n_iter][corrector_step-1])/(cfd.dict_res[n_iter][corrector_step-2])))
                        cfd.dict_asymptotic_error_constant[n_iter][corrector_step] = np.abs(cfd.dict_res[n_iter][corrector_step])/np.power(np.abs(cfd.dict_res[n_iter][corrector_step-1]),cfd.dict_approximated_order_of_convergence[n_iter][corrector_step])
                    # Averaged values
                    cfd.dict_approximated_order_of_convergence_mean[n_iter][corrector_step] = np.mean(cfd.dict_approximated_order_of_convergence[n_iter][corrector_step])
                    cfd.dict_asymptotic_error_constant_mean[n_iter][corrector_step] = np.mean(cfd.dict_asymptotic_error_constant[n_iter][corrector_step])

            ### Calculate rate of convergence, Performance is bad as it is repeated from scratch each time...
            # Infinity norm (no local resolution)
            cfd.dict_approximated_order_of_convergence_norm = copy.deepcopy(cfd.dict_norm)
            cfd.dict_asymptotic_error_constant_norm = copy.deepcopy(cfd.dict_norm)
            for n_iter in cfd.dict_norm:
                for corrector_step in cfd.dict_norm[n_iter]:
                    if corrector_step < 3:
                        cfd.dict_approximated_order_of_convergence_norm[n_iter][corrector_step] = np.nan
                        cfd.dict_asymptotic_error_constant_norm[n_iter][corrector_step] = np.nan
                    else: # n_iter > 2
                        cfd.dict_approximated_order_of_convergence_norm[n_iter][corrector_step] = np.log10(np.abs((cfd.dict_norm[n_iter][corrector_step])/(cfd.dict_norm[n_iter][corrector_step-1])))/np.log10(np.abs((cfd.dict_norm[n_iter][corrector_step-1])/(cfd.dict_norm[n_iter][corrector_step-2])))
                        cfd.dict_asymptotic_error_constant_norm[n_iter][corrector_step] = np.abs(cfd.dict_norm[n_iter][corrector_step])/np.power(np.abs(cfd.dict_norm[n_iter][corrector_step-1]),cfd.dict_approximated_order_of_convergence_norm[n_iter][corrector_step])

            ## Prepare convergence logs header
            if cpl.n_iter == 1 and cpl.corrector_step == 1:
                
                with open(cpl.path + '/log.cvg', 'w') as f:
                    f.write('# Coupling Interval, '
                            + 'Corrector Step (max ' + str(cpl.n_outer_correctors) + '), '
                            + 'CFD norm ('+ str(cfd.convergence_norm) + '), '
                            + 'CFD limit, ' 
                            + 'CFD converged, '
                            + 'CFD xyz of max deviation, '
                            + 'Order of convergence (inf norm), '
                            + 'Asymptotic error constant (inf norm)'
                            + '\n')    
    
            ## Append convergence log
            with open(cpl.path + '/log.cvg', 'a') as f:
                f.write(str(cpl.n_iter) + ', ' 
                        + str(cpl.corrector_step) + ', '
                        + str("{0:.12f}".format(cfd.norm)) + ', '
                        + str(cfd.convergence_limit) + ', '
                        + str(cfd.converged)+ ', '
                        + '('
                        + str("{0:.6f}".format(cfd.input[cfd.norm_max_index, 0])) + ' | '
                        + str("{0:.6f}".format(cfd.input[cfd.norm_max_index, 1])) + ' | '
                        + str("{0:.6f}".format(cfd.input[cfd.norm_max_index, 2])) + ' ) ' + ', '
                        + str("{0:.6f}".format(cfd.dict_approximated_order_of_convergence_norm[cpl.n_iter][cpl.corrector_step])) + ', '
                        + str("{0:.6f}".format(cfd.dict_asymptotic_error_constant_norm[cpl.n_iter][cpl.corrector_step]))
                        + '\n')

#            ## Append infinity convergence log
#            with open(cpl.path + '/log.cvg.inf', 'a') as f:
#                f.write(str(cpl.n_iter) + ', ' + str(cpl.corrector_step) + ', '
#                        + str("{0:.8f}".format(fea.norm_inf)) + ', '
#                        + str("{0:.8f}".format(cfd.norm_inf)) + ', ('
#                        + str("{0:.6f}".format(fea.input[fea.norm_max_index, 0])) + ' | '
#                        + str("{0:.6f}".format(fea.input[fea.norm_max_index, 1])) + ' | '
#                        + str("{0:.6f}".format(fea.input[fea.norm_max_index, 2])) + ' ), ('
#                        + str("{0:.6f}".format(cfd.input[cfd.norm_max_index, 0])) + ' | '
#                        + str("{0:.6f}".format(cfd.input[cfd.norm_max_index, 1])) + ' | '
#                        + str("{0:.6f}".format(cfd.input[cfd.norm_max_index, 2])) + ' )\n')

            # Adjust interval size at end of corrector step.
            cpl.set_time('end_corrector_step',fea)

            # Set control variable cpl.converged == True in case of convergence
#            if fea.converged and cfd.converged == True:
            if (cfd.converged == True) and (cpl.user_converged == True):
                cpl.converged = True

            # Check if max number of corrector steps or convergence is reached
            if (cpl.corrector_step == cpl.n_outer_correctors
                    or cpl.converged == True):
                cpl.final_corrector_step = True

            # Execute post-run file modifications that need convergence info
            fea.post_run()
            cfd.post_run()

            # Print convergence info and reset boolean switches
            if cpl.final_corrector_step == True:
                if cpl.converged == False:
                    # Print warning if convergence is not reached
                    logging.warning('No converged interface after '
                                    + str(cpl.corrector_step)
                                    + ' (max ' + str(cpl.n_outer_correctors)
                                    + ') corrector steps'
                                    + ' [of Coupling Interval Number: '
                                    + str(cpl.n_iter) + ']')
                elif cpl.converged == True:
                    # Print summary of this corrector step
                    print('COMPLETED: Coupling Corrector Step: '
                          + str(cpl.corrector_step) + ' (max '
                          + str(cpl.n_outer_correctors) + ')'
                          + ' [of Coupling Interval Number: '
                          + str(cpl.n_iter) + ']')
                cpl.converged = False # Reset value for next cpl iteration
                cfd.converged = False # Reset value for next cpl iteration (otherwise it is True at the beginning of the first corrector step, needed for userFunction dynamic coupling interval size)
                cpl.user_converged == True # Reset value for next cpl iteration, default is true if userFunction.py does not send a delay convergence signal (cpl.user_converged == False)
            cpl.corrector_step += 1 # Increase counter

    elif cpl.mode == 'parallel':
        sys.exit('Parallel mode was deleted as it was not updated. See legacy version.')
    else:
        sys.exit('Mode not available. Valid options: sequential, parallel.')

    # Update current simulation time by previous time interval size
    # Assume that only fea is transient and determines physical time
#    cpl.simtime['currentSimTime'] += fea.cpl_interval
    cpl.simtime['breakpoints'].append(cpl.simtime['currentSimTime']) 
    print('Total simulation time at end of coupling interval number', str(cpl.n_iter), ': t =', cpl.simtime['currentSimTime'], "s")
    print('List of coupling time instants ', cpl.simtime['breakpoints'])
    with open(cpl.path + '/coupling_instants.txt','w') as f:
        f.write('\n'.join(str(line) for line in cpl.simtime['breakpoints']))
    
    
    # Check if runtime modification is requested
    if os.path.exists(cpl.path + '/inputRuntimeChange.inp'):
        print('Runtime modification requested.')
        import configparser
        config = configparser.ConfigParser()
        config.read(cpl.path + '/inputRuntimeChange.inp')
        cpl.max_iter = float(config.get('cpl', 'maxIter'))
        cpl.backup = int(config.get('cpl', 'backup', fallback=cpl.backup))
        cpl.n_outer_correctors = int(config.get('cpl', 'nOuterCorrectors', fallback=cpl.n_outer_correctors))
#        fea.convergence_limit = float(config.get('fea', 'convergenceLimit', fallback=fea.convergence_limit))
        cfd.convergence_limit = float(config.get('cfd', 'convergenceLimit', fallback=cfd.convergence_limit))

    # Write backup
    if cpl.n_iter%cpl.backup == 0:
        import tarfile
        os.chdir(cwd)
        with tarfile.open(fea.path + '.backup.' + str(cpl.n_iter) + '.tar', 'w') as tar:
            for name in [str(fea.path)]:
                tar.add(name)

    cpl.n_iter += 1
    print('Current coupling loop time =', time.time()
          - cpl.runtime['startCplLoop'], 's.')
    print('Elapsed time so far =', time.time()
          - cpl.runtime['startCpl'], 's.')

    logging.info('Execute user functions.')
    logging.info('TODO: Import module that contains user functions.')

#### Calculate convergence properties in postprocessing
#cfd.dict_approximated_order_of_convergence = copy.deepcopy(cfd.dict_res) # zero-filled array of same size as dict_res
#cfd.dict_asymptotic_error_constant = copy.deepcopy(cfd.dict_res) # zero-filled array of same size as dict_res
#for n_iter in cfd.dict_res:
#    for corrector_step in cfd.dict_res[n_iter]:
#        if corrector_step < 3:
#            cfd.dict_approximated_order_of_convergence[n_iter][corrector_step][:] = np.nan
#            cfd.dict_asymptotic_error_constant[n_iter][corrector_step][:] = np.nan
#        else: # n_iter > 2
#            cfd.dict_approximated_order_of_convergence[n_iter][corrector_step] = np.log10(np.abs((cfd.dict_res[n_iter][corrector_step])/(cfd.dict_res[n_iter][corrector_step-1])))/np.log10(np.abs((cfd.dict_res[n_iter][corrector_step-1])/(cfd.dict_res[n_iter][corrector_step-2])))
#            cfd.dict_asymptotic_error_constant[n_iter][corrector_step] = np.abs(cfd.dict_res[n_iter][corrector_step])/np.power(np.abs(cfd.dict_res[n_iter][corrector_step-1]),cfd.dict_approximated_order_of_convergence[n_iter][corrector_step])
#        # Averaged values
#        cfd.dict_approximated_order_of_convergence_mean[n_iter][corrector_step] = np.mean(cfd.dict_approximated_order_of_convergence[n_iter][corrector_step])
#        cfd.dict_asymptotic_error_constant_mean[n_iter][corrector_step] = np.mean(cfd.dict_asymptotic_error_constant[n_iter][corrector_step])


### TODO: raise exception from this point on
# Archive OpenFOAM postProcessing dir (it may contain many small files)
if tarball == True: # Archive files to avoid many small files (disk performance)
    if cfd.name in ['OF4', 'OF6']:
        cmd = ('pushd ' + cfd.path + '/postProcessing > /dev/null && '
               '/bin/tar -cf ../postProcessing.tar . && cd .. && '
               'rm -rf postProcessing && popd > /dev/null')
        subprocess.check_call(cmd, shell=True)


print('Prototype: Some immediate convergence printing for LaTeX')
print('\nIterations | Fea Norm')
for iterations in fea.dict_norm[1]:
    print(str(iterations) + ' ' + str(fea.dict_norm[1][iterations]))
    
print('\nIterations | Cfd Norm')
for iterations in cfd.dict_norm[1]:
    print(str(iterations) + ' ' + str(cfd.dict_norm[1][iterations]))
if cfd.relax_method == 'none' or cfd.relax_method == 'constant':
    # Only valid for constant relaxation parameter (not Aitken)
    print('\nRelaxation Coefficient omega2 | Asymptotic error constant')
    for iterations in cfd.dict_asymptotic_error_constant_norm[1]:
        print(str(cfd.relax_omega_init) + ' ' + str(cfd.dict_asymptotic_error_constant_norm[1][iterations]))
    print('\nRelaxation Coefficient omega2 | Order of convergence')
    for iterations in cfd.dict_approximated_order_of_convergence_norm[1]:
        print(str(cfd.relax_omega_init) + ' ' + str(cfd.dict_approximated_order_of_convergence_norm[1][iterations]))

## End notes / summary
print('\nSummary:')
print('Total coupling time =', time.time() - cpl.runtime['startCpl'], 's.')
print('Total FEA time =', fea.runtime['totalLoop'], 's.')
print('Total CFD time =', cfd.runtime['totalLoop'], 's.')
print('Total runtime:', time.time() - cpl.runtime['startMaster'], 's.')

## End PROFILING
if PROFILING == True:
    pr.disable()
    strng = io.StringIO()
    sortby = 'cumulative'
    ps = pstats.Stats(pr, stream=strng).sort_stats(sortby)
    ps.print_stats()
    print(strng.getvalue())
