#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 12:02:10 2021

Plot residuals of coupling code.

@author: asc
"""

import numpy as np
import os
import sys
import matplotlib.ticker
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import tikzplotlib

if __name__ == '__main__':   
    # Set cwd
    cwd = os.getcwd()
    print('Current working directory:', cwd)
    input_path = cwd + '/log.cvg'
    
    if os.path.exists(input_path):
        print('Read and plot ' + input_path)
        header = np.genfromtxt(input_path, skip_header=0, max_rows=1, delimiter=',', comments='//', dtype='str')
        data = np.genfromtxt(input_path, skip_header=1, delimiter=',', autostrip=True)
    else:
        sys.exit('Error: ' + input_path + ' does not exist or cannot be read.')

    
    cplStep = data[:,0]
    nu = data[:,1]
    norm = data[:,2]
    limit = data[:,3]
    
    
    fig,ax = plt.subplots(constrained_layout=True)
    ax.plot(norm,'-')
    ax.plot(limit,'r-')
    plt.yscale('log')
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12) 
    ax.yaxis.set_major_locator(locmaj)
    plt.legend([header[2], header[3]])
    plt.grid(color='k', linestyle='--', linewidth=1)
    plt.show()
