#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This module sets the correct simulation environment.
"""

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import os
import subprocess
import sys

def source_code(name):
    """ This function sets the correct simulation environment.
    TODO: For more flexibility one could read cmd from input file and pass it
    directly to this function. Check first, if there are security issues, when
    the user's (arbitrary) bash commands are executed by this program.
    """
    if name == 'ccx':
        cmd = None # Not necessary for ccx
    elif name == 'OF4':
        cmd = ('/bin/bash -c "source /export/home/storage/Software/Linux/'
               'OpenFOAM/OpenFOAM-4.x/etc/bashrc; env"')
    elif name == 'OF6':
        cmd = ('/bin/bash -c "source /home/asc/'
               'OpenFOAM/OpenFOAM-6/etc/bashrc; env"')
#        cmd = ('/bin/bash -c "source /export/home/storage/Software/Linux/'
#               'OpenFOAM/OpenFOAM-6/etc/bashrc; env"')
    elif name == 'CFX18':
        cmd = ('/bin/bash -c "PATH=/sw/ANSYS_19/ansys_inc/v190/CFX/bin:'
               '$PATH; env"')
    else:
        sys.exit('###\nError: No environment specified for ' + name
                 + '\n###')

    if cmd is not None:
        try:
            pipe = subprocess.Popen(cmd.encode('utf-8'),
                                    stdout=subprocess.PIPE, shell=True)
            out = pipe.communicate(timeout=1)[0] # only stout
            env = dict((line.split('=', 1) for line in out.decode('utf-8')
                        .splitlines()))
            logging.debug('env:' + str(env))
            os.environ.update(env)
            print(name + ': Environment set.')
        except:
            sys.exit('###\nError: Cannot set environment for ' + name
                     + '\n###')
    else:
        print(name + ': No sourcing done or not required.')
    return None

