#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This is the initializing script for FEA/CFD. """

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def initialize(code):
    """ Function initializes variables and writes files that are needed prior
    to the coupling loop"""
    ## Initialize convertformat and start values
    print(code.name, ': Initialize interface patches using python.')
    code.convert_format_to_uni(read_mesh=True, variable='None')
    code.write_format(init_field=True, variable=code.input_variable)
    return None
