#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This module checks convergence. """

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def check_convergence(vec_old, vec_new, limit=1e-3, p_norm='np.inf'):
    """
    Input arguments: two vectors vec_old, vec_new, convergence limit
    and order of norm
    Return: boolean (convergence limit reached?), norm, norm_max_index
    """
    # Import modules
    import numpy as np
    import logging

    # Signed difference of new and old values
    res = np.subtract(vec_new, vec_old)
    # Absolute difference of new and old values
    vec_diff = np.absolute(np.subtract(vec_new, vec_old))

    # Choose norm
    logging.debug('Check whether infinity norm is suitable as default.')
    if p_norm == 'np.inf': # Default value
        order = np.inf     # Infinity norm
    elif p_norm == 'rms': # Root mean square
        pass
    else:
        try:
            order = int(p_norm)     # p-norm where p = 1,2,3,...
            # order = None     # 2-norm
        except ValueError:
            logging.error('Cannot convert p_norm to integer.')
            raise

    # Calculate p-norm
    if p_norm == 'rms':
        norm = np.sqrt(np.vdot(vec_diff, vec_diff)/vec_diff.size)
    else:
        norm = np.linalg.norm(vec_diff, order)

    # Check if norm is smaller than convergence limit
    if norm <= limit:
        converged = True
    else:
        converged = False

    # Debug: Calculate some more norms
    logging.debug('norm(z, np.inf): ' + str(np.linalg.norm(vec_diff, np.inf)))
    logging.debug('norm(z, 0): ' + str(np.linalg.norm(vec_diff, 0)))
    logging.debug('norm(z, 1): ' + str(np.linalg.norm(vec_diff, 1)))
    logging.debug('norm(z, 2): ' + str(np.linalg.norm(vec_diff, 2)))
    logging.debug('rms(z): ' + str(np.sqrt(np.vdot(vec_diff, vec_diff)
                                           /vec_diff.size)))

    # Index of maximum deviation (i. e. infinity norm) to detect faces with bad
    # convergence behavior
    norm_max_index = np.argmax(np.absolute(vec_diff))

    # Return convergence status and norm
    return converged, norm, norm_max_index, res
