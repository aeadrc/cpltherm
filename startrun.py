#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This is the run script for FEA/CFD """
# Add new code through additional elif entry

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time
import subprocess
import logging

def start(code, cpl):
    """ This function prepares the start files and runs the FEA/CFD codes. """
    import sys, os, shutil, re
    ###########################################################################
    ## CalculiX
    ###########################################################################
    if code.name == 'ccx':
        print(code.name + ': Start FEA calculation.')
        # + code.startFrom + 's to ' + str(code.stopAt) + 's.')
        os.chdir(code.path) # Change to dir where commands have to be executed

        # Modify input file
        if cpl.n_iter == 1 and code.restart == False:
            # Use start .inp file only for fresh coupling start if there are
            # no results from a previous calculation
            control_dict = code.path + '/' + code.start_file + '.inp'
        else:
            # Restart .inp file is used for restart of whole coupling as well
            # as for coupling iterations 2,3,...
            control_dict = code.path + '/' + code.restart_file + '.inp'
        if cpl.n_iter == 1 and cpl.corrector_step == 1 and code.restart == True:
            try:
                # Copy locked restart file. This ensures that *.rin.lock is
                # not deleted when cleaning directory and that correct *.rin
                # file is used for restart
                logging.info(code.name + ': Copy ' + code.restart_file
                             + '.rin.lock to ' + code.path + '/'
                             + code.restart_file + '.rin')
                shutil.copy2(code.path + '/' + code.restart_file
                             + '.rin.lock', code.path + '/'
                             + code.restart_file + '.rin')
                # Copy locked restart interface values
                if code.input_variable == 'heatFlux':
                    if os.path.exists(code.path + '/' + code.patch_name
                                      + '.dfl.lock'):
                        logging.info(code.name + ': Copy ' + code.patch_name
                                     + '.dfl.lock to ' + code.patch_name
                                     + '.dfl')
                        # TODO: This workaround is why the first convergence
                        # check is wrong, because the initialization is
                        # overwritten, but still used for convergence calc
                        shutil.copy2(code.path + '/' + code.patch_name
                                     + '.dfl.lock', code.path + '/'
                                     + code.patch_name + '.dfl')
                    else:
                        logging.warning('Cannot find ' + code.path + '/'
                                        + code.patch_name + '.dfl.lock')
                        logging.warning('Use initial values instead.')
                elif code.input_variable == 'Robin':
                    if os.path.exists(code.path + '/' + code.patch_name
                                      + '.flm.lock'):
                        logging.info(code.name + ': Copy '
                                     + code.patch_name + '.flm.lock to '
                                     + code.patch_name + '.flm')
                        # TODO: This workaround is why the first convergence
                        # check is wrong, because the initialization is
                        # overwritten, but still used for convergence calc
                        shutil.copy2(code.path + '/' + code.patch_name
                                     + '.flm.lock', code.path + '/'
                                     + code.patch_name + '.flm')
                    else:
                        logging.warning('Cannot find ' + code.path + '/'
                                        + code.patch_name + '.flm.lock')
                        logging.warning('Use initial values instead.')
                else:
                    logging.warning('Restart from patch only implemented for '
                                    'heatFlux and Robin boundary.')
            except:
                sys.exit('Error: Cannot copy *.lock files for restart.')
        try:
            file = open(control_dict, 'r')
            strng = file.read()
            # startFile/restartFile contains only last *STEP,
            # previous *STEPs are included through steps.inp
            if code.transient_bc == True and cpl.corrector_step > 1:
                strng = re.sub(r'^\*HEAT TRANSFER,DIRECT.*\n.*$',
                               '*HEAT TRANSFER,DIRECT\n' + str(code.delta_t)
                               + ',' + str(code.delta_t), strng, flags=re.M)
                #Check if input file contains '*include, input=steps.inp'
                if not '*include, input=steps.inp' in strng:
                    logging.warning('ccx: There is no "*include, '
                                    'input=steps.inp" in input file '
                                    + str(control_dict) + ' for transient BC.')
            # backwards compatibility when BC is constant for cplInterval
            else:
                strng = re.sub(r'^\*HEAT TRANSFER,DIRECT.*\n.*$',
                               '*HEAT TRANSFER,DIRECT\n' + str(code.delta_t)
                               + ',' + str(code.cpl_interval), strng,
                               flags=re.M)
            file.close()
            file = open(control_dict, 'w')
            file.write(strng)
            file.close()
            # NOTE: For steady ccx nothing is modified.
            # *HEAT TRANSFER,STEADY STATE is not substituted
        except:
            sys.exit('Error: Cannot open file for reading/writing: '
                     + control_dict)

        # Set parallel environment settings
        os.environ['OMP_NUM_THREADS'] = str(code.n_procs)
        #os.system('/bin/bash -c "echo $OMP_NUM_THREADS"')

        # Distinguish between first and subsequent coupling loops
        if cpl.n_iter == 1 and code.restart == False:
            print('First coupling loop.')
            if code.result_format == 'exo':
                # Only available if CalculiX Extras is available
                cmd = ('ccx ' + code.start_file + ' -o exo >> ' + code.path
                       + '/' + code.log + ' 2>&1')
            else:
                cmd = ('ccx ' + code.start_file + ' >> ' + code.path
                       + '/'+ code.log + ' 2>&1')
            print(code.name + ': ' + cmd)
            subprocess.check_call(cmd, shell=True)
        else:
            print('Not first coupling loop.')
            if code.result_format == 'exo':
                # Only available if CalculiX Extras is available
                cmd = ('ccx ' + code.restart_file + ' -o exo >> ' + code.path
                       + '/' + code.log + ' 2>&1')
            else:
                cmd = ('ccx ' + code.restart_file + ' >> ' + code.path
                       + '/' + code.log + ' 2>&1')
            print(code.name + ': ' + cmd)
            check_time = time.time()
            subprocess.check_call(cmd, shell=True)
            print('runtime =', time.time() - check_time, 's.')

    ###########################################################################
    ## OpenFOAM
    ###########################################################################
    elif code.name == 'OF6':
        print(code.name + ': Start CFD calculation from ' + code.start_from
              + 's to ' + str(code.stopAt) + 's.')
        # Change to dir where commands have to be executed
        os.chdir(code.path)

        if cpl.n_iter == 1 and cpl.corrector_step == 1:
            # Choose OpenFOAM solver only at start
            try:
                # Get OpenFOAM solver application from controlDict
                # TODO: Better to use OF environment variables!
                cmd = ('sed -ne "s/^ *application\s*\([a-zA-Z]*\)\s*;.*$/\\1/p" '
                       + code.path + '/system/controlDict')
                pipe = subprocess.Popen(cmd.encode('utf-8'),
                                        stdout=subprocess.PIPE, shell=True)
                out = pipe.communicate(timeout=10)[0] # only stout
                code.solver = str(out.rstrip(), 'utf-8')
            except:
                sys.exit('Error: Cannot get OpenFOAM application.')
        print(code.solver)
        check_time = time.time()
        # Already written

        if code.n_procs == 1:
            cmd = code.solver + ' > ' + code.path + '/log 2>&1'
            print(code.name + ': ' + cmd)
            subprocess.check_call(cmd, shell=True)

        elif code.n_procs > 1: # Run in parallel
            if cpl.n_iter == 1 and cpl.corrector_step == 1:
                # Prepare numProcs for OpenFOAM parallel
                # The system/numProcs file must be included in the decomposeParDicts
                with open(code.path + '/system/numProcs', 'w') as file:
                    strng = ('numberOfSubdomains ' + str(code.n_procs) + ';')
                    file.write(strng)
                # Decompose Domain and create processor* directories
                cmd = ('decomposePar -force -allRegions -time '
                       + str(code.start_from) + ' > ' + code.path
                       + '/log.decomposePar 2>&1')
                # TODO: Switch for non-cht solvers (no -allRegions)
                print(code.name + ': ' + cmd)
                subprocess.check_call(cmd, shell=True)
            # Start MPI OpenFOAM
            cmd = ('/usr/lib64/mpi/gcc/openmpi/bin/mpirun -np '
                   + str(code.n_procs) + ' ' + code.solver + ' -parallel > '
                   + code.path + '/log 2>&1')
            print(code.name + ': ' + cmd)
            subprocess.check_call(cmd, shell=True)

#             cmd = ('reconstructPar -allRegions -latestTime > '
#                    + code.path + '/log.reconstructPar 2>&1')
#             print(code.name + ': ' + cmd)
#             subprocess.check_call(cmd, shell=True)
            # Delete old processor* directories
            #os.remove(code.path + '/processor*')

        else:
            sys.exit('Error: Specify nProcs for ' + code.name)

    ############################################################################
    ## CFX
    ############################################################################
    elif code.name == 'CFX18':
        os.chdir(code.path) # Change to dir where commands have to be executed
        print(code.name + ': Start CFD calculation.')

        if cpl.n_iter == 1: #for all cpl.correctorSteps
            # At first cpl.n_iter: Initialization with initFlowAnalysis.pre
            # At all correctorSteps of first cpl.n_iter:
            # no code.startFile.latestTime.res available
            print('First coupling loop.')
            if cpl.corrector_step == 1:
                logging.debug('First Corrector Step')

                # DUMMY for fixes in the future:
                # Distinguish between steady and transient CFD
                if True == True:
                    # Modify command file: Init field and Boundary Conditions
                    # according to specified values only at first iteration
                    control_dict = code.path + '/initFlowAnalysis.pre'
                    try:
                        file = open(control_dict, 'r')
                        strng = file.read()
                        strng = re.sub(r'^      Time per Run =.*$',
                                       '      Time per Run = '
                                       + str(code.cpl_interval) + ' [s]',
                                       strng, flags=re.M)
                        strng = re.sub(r'^      Timesteps =.*$',
                                       '      Timesteps = '
                                       + str(code.delta_t) + ' [s]',
                                       strng, flags=re.M)
                        file.close()
                        file = open(control_dict, 'w')
                        file.write(strng)
                        file.close()
                    except:
                        sys.exit('Error: Cannot open file for r/w: '
                                 + control_dict)
                else:
                    pass
                # DUMMY for fixes in the future:
                # Distinguish between steady and transient CFD

                # Prepare first iteration
                logging.warning('cfx5pre is executed only at first cpl.n_iter.'
                                ' If I want to update timestep information for'
                                ' transient runs -> implement it!')
                cmd = ('cfx5pre -cfx ' + code.start_file
                       + '.cfx -batch initFlowAnalysis.pre > '
                       + code.path + '/log.initFlowAnalysis 2>&1')
                print(code.name + ': ' + cmd)
                subprocess.check_call(cmd, shell=True)

            # Allow restart from prev. CFX calc, results name = *_restart.res
            if code.restart == False:
                # Execute cfx run
                if code.n_procs == 1:
                    cmd = 'cfx5solve -def ' + code.start_file + '.def'
                elif code.n_procs > 1:
                    cmd = ('cfx5solve -def ' + code.start_file
                           + '.def -part ' + str(code.n_procs)
                           + ' -start-method "Intel MPI Local Parallel"')
                else:
                    sys.exit('Error: Set nProcs.')

            elif code.restart == True:
                if cpl.corrector_step == 1:
                    # Unlock restart file now (it is locked so it is not
                    # deleted at initial cleaning.)
                    logging.info(code.name + ': Copy ' + code.start_file
                                 + '_restart.res.lock to ' + code.path + '/'
                                 + code.start_file + '_restart.res')
                    shutil.move(code.path + '/' + code.start_file
                                + '_restart.res.lock', code.path + '/'
                                + code.start_file + '_restart.res')
                    # Unlock interface restart conditions
                    logging.warning('TODO: Implement restart CFD interface, '
                                    'only important for parallel mode, for '
                                    'sequential mode interface is calculated by'
                                    ' previous fea anyway.')

                # Execute CFX run
                if code.n_procs == 1:
                    print(code.name + ': Serial run.')
                    cmd = ('cfx5solve -def ' + code.start_file
                           + '.def -continue-from-file '
                           + code.start_file + '_restart.res')
                elif code.n_procs > 1:
                    print(code.name + ': Parallel run using '
                          + str(code.n_procs) + ' cores.')
                    cmd = ('cfx5solve -def ' + code.start_file
                           + '.def -continue-from-file ' + code.start_file
                           + '_restart.res -part ' + str(code.n_procs)
                           + ' -start-method "Intel MPI Local Parallel"')
                else:
                    sys.exit('Error: Set nProcs.')

        elif cpl.n_iter > 1:
            print('Not first coupling and not first or second corrector loop.')
            logging.warning('Implement update of timesteps and cfx5pre execu'
                            'tion, but only for transient runs (performance)')

            # Execute cfx run
            if code.n_procs == 1:
                print(code.name + ': Serial run.')
                cmd = ('cfx5solve -def ' + code.start_file
                       + '.def -continue-from-file ' + code.start_file
                       + '_latestTime.res')
            elif code.n_procs > 1:
                print(code.name + ': Parallel run using ' + str(code.n_procs)
                      + ' cores.')
                cmd = ('cfx5solve -def ' + code.start_file
                       + '.def -continue-from-file ' + code.start_file
                       + '_latestTime.res -part ' + str(code.n_procs)
                       + ' -start-method "Intel MPI Local Parallel"')
            else:
                sys.exit('Error: Set nProcs.')

        # For all cpl.n_iter steps (first and following)
        cpl.runtime['startCFXLoop'] = time.time()
        try:
            print(code.name + ': ' + cmd)
            subprocess.check_call(cmd, shell=True)
        except:
            # Try again if there is a temporary licensing error
            print(code.name + ': Check if there was a licensing error.')
            try:
                with open(code.path + '/' + code.start_file
                          + '_001.out') as file:
                    if ('The solver is unable to continue because of licensing'
                        ' problems.' in file.read()):
                        if os.path.exists(code.path + '/' + code.start_file
                                          + '_001'):
                            shutil.rmtree(code.path + '/' + code.start_file
                                          + '_001')
                        logging.warning(code.name+ ': Licensing error occurred,'
                                        ' try again in 61 s.')
                        # Wait and hope license server can be reached now
                        time.sleep(61)
                        print(code.name + ': ' + cmd)
                        subprocess.check_call(cmd, shell=True)
                    else:
                        logging.error(code.name + ': It looks like licensing '
                                      'was not the cause for the crash.')
                        raise
                os.remove(code.path + '/' + code.start_file + '_001.out')
            except:
                logging.error(code.name + ': An error occurred when trying to '
                              'run CFX again.')
                raise

        print('CFX: Pure execution time =',
              time.time() - cpl.runtime['startCFXLoop'], 's.')

        # Postprocessing: Export heat flux
        cmd = ('cfx5post -res ' + code.start_file
               + '_001.res -batch exportHeatFlux.cse')
        print(code.name + ': ' + cmd)
        subprocess.check_call(cmd, shell=True)

    else:
        sys.exit('\n###Error: Code ' + code.name + ' not yet implemented.')

    os.chdir(cpl.path) # Change to dir where commands have to be executed
    return None
