#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This is the interpolation module. """

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import subprocess
import numpy as np
from scipy import interpolate
import logging
logger = logging.getLogger(__name__)

################################################################################
## Function interpolate_values
################################################################################
def interpolate_values(source, target, variable, cpl, tarball):
    """
    Interpolates x, y, z, val from source to target (both in universal format)
    """
    # Update target format
    logging.debug('Update target x, y, z (depending on where variable is stored '
                  '(facecenter, vertex))')

    # Copy input of previous coupling iteration, so that it can be used for
    # temporal interpolation / underrelaxation
    if cpl.corrector_step == 1:
        target.inputPrev = np.copy(target.input) # Not used yet.

    # Copy input of old corrector or previous cpl step for convergence check
    target.input_old = np.copy(target.input)

    # DEBUG!
    logging.debug('target.input_old' + str(target.input_old))
    logging.debug('target.input' + str(target.input))

    if target.name == 'ccx':
        if variable == 'heatFlux' or 'Robin':
            print(target.name + ': Get input format for ' + variable)
            # heatFlux and film bc are applied at face centers in ccx
            # Update face center coordinates
            target.input[:, 0:3] = np.column_stack(
                [target.barycenter_vector['x'],
                 target.barycenter_vector['y'],
                 target.barycenter_vector['z']])
            logging.debug(str(target.input))
        else:
            sys.exit('Error: ' + target.name + ': Input for ' + variable
                     + ' not yet implemented.')
    elif target.name == 'OF4':
        if variable == 'temperature':
            print(target.name + ': Get input format for ' + variable)
            # Temperature is applied at face centers in OF4
            # Update face center coordinates
            target.input[:, 0:3] = np.column_stack(
                [target.barycenter_vector['x'],
                 target.barycenter_vector['y'],
                 target.barycenter_vector['z']])
            logging.debug(str(target.input))
        else:
            sys.exit('Error: ' + target.name + ': Input for ' + variable
                     + ' not yet implemented.')
    elif target.name == 'OF6':
        pass
        # Comment for quick fix
#         if variable == 'temperature':
#             print(target.name + ': Get input format for ' + variable)
#             # Temperature is applied at face centers in OF4
#             # Update face center coordinates
#             target.input[:, 0:3] = np.column_stack(
#                 [target.barycenter_vector['x'],
#                  target.barycenter_vector['y'],
#                  target.barycenter_vector['z']])
#             logging.debug(str(target.input))
#         else:
#             sys.exit('Error: ' + target.name + ': Input for ' + variable
#                      + ' not yet implemented.')
    elif target.name == 'CFX18':
        if variable == 'temperature':
            print(target.name + ': Get input format for ' + variable)
            # Temperature is applied at nodes in CFX18
            # Update face center coordinates
            target.input[:, 0:3] = np.column_stack(
                [target.vertices_vector['x'],
                 target.vertices_vector['y'],
                 target.vertices_vector['z']])
            logging.debug(str(target.input))
        else:
            sys.exit('Error: ' + target.name + ': Input for ' + variable
                     + ' not yet implemented.')
    else:
        sys.exit('Error: Target name ' + target.name + ' not yet implemented.')

    # Interpolation from (x, y, z)Source to (x, y, z)Target
    print('\nInterpolation from ' + source.name + ' to ' + target.name + '.')
    print('PROTOTYPE: There is only "nearest" extrapolation available...')

    x_source = source.output[:, 0]
    y_source = source.output[:, 1]
    z_source = source.output[:, 2]
    val_source = source.output[:, 3]
    if variable == 'Robin':
        # Only needed for variable h_coeff
        h_coeff_source = source.output[:, 4]
        q_source = source.output[:, 5]
    x_target = target.input[:, 0] # Check if this is reasonable
    y_target = target.input[:, 1] # Check if this is reasonable
    z_target = target.input[:, 2] # Check if this is reasonable

    # Scipy griddata interpolation
    if cpl.interface_surface == 'nonplanar':
        print('Perform 3D interpolation with ' + cpl.interpolation_method)
#         val_target = interpolate.griddata((x_source, y_source, z_source),
#                                           val_source,
#                                           (x_target, y_target, z_target),
#                                           method='nearest',
#                                           fill_value='extrapolate')
        # Try linear in 3D
        if cpl.interpolation_method == 'griddata':
            val_target_nearest = interpolate.griddata(
                (x_source, y_source, z_source), val_source,
                (x_target, y_target, z_target), method='nearest')
            val_target = interpolate.griddata(
                (x_source, y_source, z_source), val_source,
                (x_target, y_target, z_target), method='linear', fill_value='nan')
            logging.debug(str(val_target))
            for ind in range(len(val_target)):
                if np.isnan(val_target[ind]):
                    val_target[ind] = val_target_nearest[ind]
            if variable == 'Robin':# and target.variable_h_coeff == True:
                # Only for convergence check
                q_target_nearest = interpolate.griddata(
                    (x_source, y_source, z_source), q_source,
                    (x_target, y_target, z_target), method='nearest')
                q_target = interpolate.griddata(
                    (x_source, y_source, z_source), q_source,
                    (x_target, y_target, z_target), method='linear', fill_value='nan')
                for ind in range(len(q_target)):
                    if np.isnan(q_target[ind]):
                        q_target[ind] = q_target_nearest[ind]
                if target.variable_h_coeff == True:
                    h_coeff_target_nearest = interpolate.griddata(
                        (x_source, y_source, z_source), h_coeff_source,
                        (x_target, y_target, z_target), method='nearest')
                    h_coeff_target = interpolate.griddata(
                        (x_source, y_source, z_source), h_coeff_source,
                        (x_target, y_target, z_target), method='linear', fill_value='nan')
                    for ind in range(len(h_coeff_target)):
                        if np.isnan(h_coeff_target[ind]):
                            h_coeff_target[ind] = h_coeff_target_nearest[ind]

        elif cpl.interpolation_method == 'rbf':
            rbfi = interpolate.Rbf(x_source, y_source, z_source, val_source,
                                   function=cpl.interpolation_method_function,
                                   smooth=cpl.interpolation_method_smooth)
            # rbfi function=cubic, gaussian, inverse_multiquadric, linear,
            #     multiquadric, quintic, thin_plate
            # rbfi smooth=0 (interpolation) or a positive value (smoothing)
            val_target = rbfi(x_target, y_target, z_target)
            if variable == 'Robin':# and target.variable_h_coeff == True:
                # Only for convergence check
                rbfi = interpolate.Rbf(x_source, y_source, z_source, q_source,
                                       function=cpl.interpolation_method_function,
                                       smooth=cpl.interpolation_method_smooth)
                q_target = rbfi(x_target, y_target, z_target)
                if target.variable_h_coeff == True:
                    rbfi = interpolate.Rbf(x_source, y_source, z_source, h_coeff_source,
                                           function=cpl.interpolation_method_function,
                                           smooth=cpl.interpolation_method_smooth)
                    h_coeff_target = rbfi(x_target, y_target, z_target)
        else:
            logging.error('No interpolation method specified.')

        logging.debug('x_source' + str(x_source))
        logging.debug('x_target' + str(x_target))
        logging.debug('y_source' + str(y_source))
        logging.debug('y_target' + str(y_target))
        logging.debug('z_source' + str(z_source))
        logging.debug('z_target' + str(z_target))
        logging.debug('val_source' + str(val_source))
        logging.debug('val_target' + str(val_target))
        if variable == 'Robin':# and target.variable_h_coeff == True:
            logging.debug('q_source' + str(q_source))
            logging.debug('q_target' + str(q_target))
            if target.variable_h_coeff == True:
                logging.debug('h_coeff_source' + str(h_coeff_source))
                logging.debug('h_coeff_target' + str(h_coeff_target))


    elif cpl.interface_surface == 'planar':
        print('Perform 2D interpolation with ' + cpl.interpolation_method)
        # Oh my... This is ugly
        # Transformation from 3D in 2D plane (necessary for
        # surface Delaunay triangulation)
        # Original base
        basis = np.transpose(np.array([(1, 0, 0), (0, 1, 0), (0, 0, 1)]))

        cpl.interp_normal_axis = 'none'
        if cpl.interp_normal_axis == 'z_y':
#            # Assume that planar surface has normal in z direction.
#            # Direction of one-dimensional points is y.
#            # Extent x points
#            if (np.max(x_source) - np.min(x_source)) < 1e-6:
#                max_target_extend = np.max(x_target) - np.min(x_target)
#                x_source_extended = np.concatenate((x_source,x_source,x_source),axis=0)
            
#            points_source = np.column_stack([x_source[:], y_source[:], z_source[:]])
#            # Normal in 0,0,1
#            normal = np.array([0,0,1])
#            normal2 = np.array([0,0,1])
#            normal3 = np.array([0,0,1])
            sys.exit('2D interpolation with z normal not implemented yet.')
        elif cpl.interp_normal_axis == 'y':
            sys.exit('2D interpolation with y normal not implemented yet.')
        elif cpl.interp_normal_axis == 'x':
            sys.exit('2D interpolation with x normal not implemented yet.')    
        else:
            # PROTOTYPE:
            points_source = np.column_stack([x_source[:], y_source[:], z_source[:]])
            # Normal vector derived from 3 (non-collinear!) points
            # Which point is min. away from origin? Take as reference
            distance_abs_from_origin = np.abs(points_source[:,0])+np.abs(points_source[:,1])+np.abs(points_source[:,2])
            min_index = np.argmin(distance_abs_from_origin)
            point_min = points_source[min_index]
            # Which point is max. away from origin?
            # First I need to make sure that this will be a different point than point_min
            mask_point_min = np.zeros(distance_abs_from_origin.size, dtype=bool)
            mask_point_min[min_index] = True # Exclude point_min as it may be that all points are equally far away from origin. We need another point, though!
            masked_distance_abs_from_origin = np.ma.array(distance_abs_from_origin, mask=mask_point_min)
            
            max_index = np.argmax(masked_distance_abs_from_origin)
            point_max = points_source[max_index]
            line_min_max = point_max - point_min
            # Which point is max. away from the line between point_min and point_max?
            distance_vector = np.zeros((len(x_source)))
            for ind, elem in enumerate(distance_vector):
                distance_vector[ind] = np.linalg.norm((point_min-points_source[ind])-(np.dot((point_min-points_source[ind]),line_min_max)*line_min_max), np.inf)
            point_three = points_source[np.argmax(distance_vector)]
            
            normal = np.cross(point_max-point_min,
                              point_three-point_min)
    #        normal = np.cross(points_source[1]-points_source[0],
    #                          points_source[2]-points_source[0])
            # Normal unit vector
            normal = np.divide(normal, np.linalg.norm(normal))
            
            # Another unit vector perpendicular to normal
            normal2 = np.divide((point_max-point_min),
                                np.linalg.norm(point_max-point_min))
    #        normal2 = np.divide((points_source[1]-points_source[0]),
    #                            np.linalg.norm(points_source[1]-points_source[0]))
            
            # Another unit vector to complete new base
            normal3 = np.cross(normal, normal2)
            # New base (z-components of all points are constant))
            basis_new = np.transpose(np.array([normal2, normal3, normal]))
            # Transformation Matrix from original to new base
            trans = np.dot(np.linalg.inv(basis_new), np.transpose(basis))
            # Transformed points in new basis coordinates
            points_n_source = np.dot(trans, np.transpose(points_source))
    
            points_target = np.column_stack([x_target[:], y_target[:], z_target[:]])
            points_n_target = np.dot(trans, np.transpose(points_target))

        # Check if z coordinate is constant
        logging.debug('points_n_source')
        logging.debug(str(points_n_source))
        logging.debug('points_n_target')
        logging.debug(str(points_n_target))
        logging.debug(str(points_n_target[2, :]))
        print('Max Deviation of planar surface: ' + str(np.max(
            points_n_target[2, :]) -  np.min(points_n_target[2, :])))
        if ((np.max(points_n_target[2, :]) -  np.min(points_n_target[2, :]))
            > 1e-5): sys.exit('Error: Target plane not 2D')

        if cpl.interpolation_method == 'griddata':
            val_target_nearest = interpolate.griddata(
                (points_n_source[0, :], points_n_source[1, :]), val_source,
                (points_n_target[0, :], points_n_target[1, :]), method='nearest')
            val_target = interpolate.griddata(
                (points_n_source[0, :], points_n_source[1, :]), val_source,
                (points_n_target[0, :], points_n_target[1, :]), method='linear',
                fill_value='nan')
            logging.debug(str(val_target))
            for ind in range(len(val_target)):
                if np.isnan(val_target[ind]):
                    val_target[ind] = val_target_nearest[ind]
            if variable == 'Robin':# and target.variable_h_coeff == True:
                # Only for convergence check
                q_target_nearest = interpolate.griddata(
                    (points_n_source[0, :], points_n_source[1, :]), q_source,
                    (points_n_target[0, :], points_n_target[1, :]), method='nearest')
                q_target = interpolate.griddata(
                    (points_n_source[0, :], points_n_source[1, :]), q_source,
                    (points_n_target[0, :], points_n_target[1, :]), method='linear', fill_value='nan')
                for ind in range(len(q_target)):
                    if np.isnan(q_target[ind]):
                        q_target[ind] = q_target_nearest[ind]
                if target.variable_h_coeff == True:
                    h_coeff_target_nearest = interpolate.griddata(
                        (points_n_source[0, :], points_n_source[1, :]), h_coeff_source,
                        (points_n_target[0, :], points_n_target[1, :]), method='nearest')
                    h_coeff_target = interpolate.griddata(
                        (points_n_source[0, :], points_n_source[1, :]), h_coeff_source,
                        (points_n_target[0, :], points_n_target[1, :]), method='linear', fill_value='nan')
                    for ind in range(len(h_coeff_target)):
                        if np.isnan(h_coeff_target[ind]):
                            h_coeff_target[ind] = h_coeff_target_nearest[ind]
        elif cpl.interpolation_method == 'rbf':
            logging.warning('Rbf not thorougly tested for 2D!')
            # Try scaling
            scale_0 = 1/(np.max(points_n_source[0, :])-np.min(points_n_source[0, :]))
            scale_1 = scale_0
#            scale_1 = 1/(np.max(points_n_source[1, :])-np.min(points_n_source[1, :]))
            points_n_source[0, :] = points_n_source[0, :] * scale_0
            points_n_source[1, :] = points_n_source[1, :] * scale_1
            points_n_target[0, :] = points_n_target[0, :] * scale_0
            points_n_target[1, :] = points_n_target[1, :] * scale_1
            rbfi = interpolate.Rbf(points_n_source[0, :], points_n_source[1, :], val_source,
                                   function=cpl.interpolation_method_function,
                                   smooth=cpl.interpolation_method_smooth)#,
#                                   epsilon=0.015)
            # rbfi function=cubic, gaussian, inverse_multiquadric, linear,
            #     multiquadric, quintic, thin_plate
            # rbfi smooth=0 (interpolation) or a positive value (smoothing)
            val_target = rbfi(points_n_target[0, :], points_n_target[1, :])
            if variable == 'Robin':# and target.variable_h_coeff == True:
                sys.exit('RBF on planar surface not implemented for Robin.')
        else:
            logging.error('No interpolation method specified.')

    elif cpl.interface_surface == '1D':
        print('Perform 1D interpolation with '
              + cpl.interpolation_method)
        if cpl.interpolation_method == 'interp1d':
            if cpl.interp1d_axis == 'y':
                # Interpolation function
                interp_f = interpolate.interp1d(y_source, val_source, fill_value='extrapolate')
                val_target = interp_f(y_target)
                
                if variable == 'Robin':
                    interp_f = interpolate.interp1d(y_source, q_source, fill_value='extrapolate')
                    q_target = interp_f(y_target)
                    interp_f = interpolate.interp1d(y_source, h_coeff_source, fill_value='extrapolate')
                    h_coeff_target = interp_f(y_target)
            else:
                sys.exit('Error: Define axis for one-dimensional interpolation (interp1dAxis).')
            
            

        elif cpl.interpolation_method == 'griddata':
            val_target = interpolate.griddata((x_source, y_source, z_source),
                                              val_source,
                                              (x_target, y_target, z_target),
                                              method='nearest')
        elif cpl.interpolation_method == 'rbf':
            sys.exit('Interpolation method "rbf" not tested yet for 1D cases.')
        else:
            sys.exit('Interpolation method ' + str(cpl.interpolation_method) + ' not known. Possible choises comprise: interp1d, griddata, rbf.')
        logging.debug('val_source')
        logging.debug(str(val_source))
        logging.debug('val_target')
        logging.debug(str(val_target))
        
        

    # Update target.input with interpolated values
    target.input[:, 3] = val_target
    if variable == 'Robin':# and target.variable_h_coeff == True:
        target.input[:, 5] = q_target
        if target.variable_h_coeff == True:
            target.input[:, 4] = h_coeff_target
    logging.debug('Updated Target input:')
    logging.debug(str(target.input))
    logging.debug('Source: min(x, y, z) =(' + str(np.min(x_source)) + ', '
                  + str(np.min(y_source)) + ', ' + str(np.min(z_source)) + ')')
    logging.debug('Source: max(x, y, z) =(' + str(np.max(x_source)) + ', '
                  + str(np.max(y_source)) + ', ' + str(np.max(z_source)) + ')')
    logging.debug('Target: min(x, y, z) =(' + str(np.min(x_target)) + ', '
                  + str(np.min(y_target)) + ', ' + str(np.min(z_target)) + ')')
    logging.debug('Target: max(x, y, z) =(' + str(np.max(x_target)) + ', '
                  + str(np.max(y_target)) + ', ' + str(np.max(z_target)) + ')')
    logging.debug('Source: minValue, maxValue =' + str(np.min(val_source))
                  + ', ' + str(np.max(val_source)))
    logging.debug('Target: minValue, maxValue =' + str(np.min(val_target))
                  + ', ' + str(np.max(val_target)))

    # Create files that can be read by paraview to visualise interpolation
    # process from source to target
    print('Write results on patch to file. Format vtk (for paraview)')
    if not os.path.exists(target.path + '/debug'):
        # Create debug subdir that contains the interpolation files
        os.mkdir(target.path + '/debug')
    file = open(target.path + '/debug/log.interpolate.source.'
                + str(cpl.n_iter) + '.' + str(cpl.corrector_step) + '.csv', 'w')
    file.write('x coord, y coord, z coord, source\n')
    for ind in range(len(source.output[:, 0])):
        file.write('%.8e, %.8e, %.8e, %.12e\n' % (source.output[ind, 0],
                                                 source.output[ind, 1],
                                                 source.output[ind, 2],
                                                 source.output[ind, 3]))
    file.close()

    file = open(target.path + '/debug/log.interpolate.target.'
                + str(cpl.n_iter) + '.' + str(cpl.corrector_step) + '.csv', 'w')
    file.write('x coord, y coord, z coord, target\n')
    for ind in range(len(target.input[:, 0])):
        file.write('%.8e, %.8e, %.8e, %.12e\n' % (target.input[ind, 0],
                                                 target.input[ind, 1],
                                                 target.input[ind, 2],
                                                 target.input[ind, 3]))
    file.close()

    # Archive debug files to avoid many small files (disk performance)
    if tarball == True:
        cmd = ('pushd ' + target.path + '/debug > /dev/null && '
               + 'find log.interpolate.source.csv* | /bin/tar -rf '
               'log.interpolate.source.tar --exclude='
               '"log.interpolate.source.tar" -trans - && rm -f '
               'log.interpolate.source.csv* && popd > /dev/null')
        subprocess.check_call(cmd, shell=True)
        cmd = ('pushd ' + target.path + '/debug > /dev/null && '
               + 'find log.interpolate.target.csv* | /bin/tar -rf '
               'log.interpolate.target.tar --exclude='
               '"log.interpolate.target.tar" -trans - && rm -f '
               'log.interpolate.target.csv* && popd > /dev/null')
        subprocess.check_call(cmd, shell=True)
        # Note: The rm -f commands should be safe, as they are only executed
        # when the previous pushd etc. are executed without error (&&)

    return None

