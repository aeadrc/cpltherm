#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This module calculates the barycenter of triangles and trapezoids """

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import scipy.spatial
import sys
import logging

def calc_barycenter(points):
    """ Provide points as
        [[x1 y1 z1]
         [x2 y2 z2]
         ...]"""
    if len(points) == 3:
        logging.info('Calculate barycenter of triangle.')
        barycenter = points.mean(axis=0)

    elif len(points) == 4:
        logging.info('Calculate barycenter of trapezoid (Check if also ok for '
                     'polygon).')

        # Transform from 3D in 2D plane (for surface Delaunay triangulation)
        # Original basis
        basis = np.transpose(np.array([(1, 0, 0), (0, 1, 0), (0, 0, 1)]))
        # Normal vector derived from 3 points
        normal = np.cross(points[1]-points[0], points[2]-points[0])
        # Normal unit vector
        normal = np.divide(normal, np.linalg.norm(normal))
        # Another unit vector perpendicular to normal
        normal2 = np.divide((points[1]-points[0]), np.linalg.norm(points[1]
                                                                  -points[0]))
        # Another unit vector to complete new basis
        normal3 = np.cross(normal, normal2)
        # New basis (z-components of all points are constant))
        basis_new = np.transpose(np.array([normal2, normal3, normal]))
        # Transformation Matrix from original to new basis
        trans = np.dot(np.linalg.inv(basis_new), np.transpose(basis))
        # Transformed points in new basis coordinates
        points_new = np.dot(trans, np.transpose(points))

        logging.debug('Nodes (x,y,z): ' + str(points))
        logging.debug('Base basis: ' + str(basis))
        logging.debug('Normalized Normal Vector: ' + str(normal))
        logging.debug('normal2: ' + str(normal2))
        logging.debug('normal3: ' + str(normal3))
        logging.debug('New Base basis_new: ' + str(basis_new))
        logging.debug('Transformation Matrix trans :' + str(trans))
        logging.debug('Nodes (xNew, yNew, zNew) as Matrix (transpose again for'
                      ' correct arrays): ' + str(points_new))

        # Delaunay triangulation
        logging.info('Delaunay Triangulation.')
        # Transposed points (for Delaunay triangulation)
        points_new_transp = np.transpose(points_new)
        # Only x,y of 4 points (z must be constant in transformed system)
        points_new_transp_xy = points_new_transp[:, 0:2]
        # Delaunay triangulation
        tri = scipy.spatial.Delaunay(points_new_transp_xy)
        indices = tri.simplices
        # Vertices of triangles
        vertices = points_new_transp_xy[indices]
        # Calculate barycenter of triangles (arithmetic mean of coordinates)
        barycenters_new = vertices.mean(axis=1)
        # Calculate area as 0.5*length(normalVector)
        areas = np.multiply(np.cross
                            (np.subtract(vertices[:, 1, :], vertices[:, 0, :]),
                             np.subtract(vertices[:, 2, :], vertices[:, 0, :]),
                             axis=1), 0.5)
        # Calculate final barycenter of triangle barycenters
        # as sum(A_i*(xyz)_i)/sum(A_i)
        barycenter_new = np.divide(np.sum(np.dot(np.diag(areas)
                                                 , barycenters_new), axis=0)
                                   , areas.sum())
        # Append z coordinate in transformed system to make barycenter
        # 3 dimenensional before transforming back
        barycenter_new = np.append(barycenter_new, points_new_transp[0, 2])
        # Transform barycenter from new basis to original basis
        barycenter = np.dot(np.linalg.inv(trans), np.transpose(barycenter_new))

        logging.debug(indices)
        logging.debug(vertices)
        logging.debug(barycenters_new)
        logging.debug(areas)
        logging.debug(np.diag(areas))
        logging.debug(np.dot(np.diag(areas), barycenters_new))
        logging.debug(barycenter_new)
        logging.debug(barycenter)
        #print('PROTOTYPE: Implement check if all 4 points are actually
        # in one plane (as they should be).')
    else:
        sys.exit('Error: Either <3 vertices or >4. Calculation of barycenter '
                 'for polygons >4 not yet implemented/tested.')

    return barycenter
