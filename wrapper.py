#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This wrapper.py script starts cplTherm and can be executed in two ways:
1. Call it externally from the run directory and use command line arguments
2. Start it in the code project directory and define cwd etc. within the script
"""

"""
This file is part of the coupling program, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

## Import modules
import io
import os
from psutil import virtual_memory
import shutil
import subprocess
import sys
import time

# Set start time
time_startwrapper = time.time()
print('Start wrapper script at', time.asctime(time.localtime(time.time())))

## Parse CL arguments
#parsed_args = sys.argv[:]
#print(str(sys.argv))
#print(str(parsed_args))

# Delete /dev/shm/tmpfs or preserve it
VALID_ARGS = {'--preserve_shm'}
# Check if command line argument exists in set VALID_ARGS
arg = VALID_ARGS & set(sys.argv)
# If one valid CL arg is given (not empty or > 1)
if len(arg) == 1:
    # Archive mode (useful if many (intermediate) result files are generated)
    preserve_shm = True
else:
    # Default
    preserve_shm = False

arg = set(sys.argv)
if len(arg) == 1:
    mode = 'interactive'
else:
    mode = 'external'

# Absolute directory of wrapper.py (and all other modules)
code_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, code_path)

try:
    # Check if any additional command line arguments exist
    
    if mode == 'interactive':  # Only string reflecting the program's source
        ####################### USER INPUT ####################################
        # Manually set cwd (for debugging from project directory)
        cwd = '/home/asc/Coupling/simple_rotor_stator/ccx_OF6_cavity_reduced_steady_RD'
        # Define arguments for debugging
        codeVersion = '0.9.16'
        logFile = 'log.cpl'
        # Define additional arguments
        arguments = '--log=DEBUG'
        ################### END USER INPUT ####################################

        # Print info
        print('Current working directory:', cwd)
        print('No CL arguments have been set. Use default.')
        print('Additional arguments: ', arguments)
    elif mode == 'external':
        # Set cwd
        cwd = os.getcwd()
        print('Current working directory:', cwd)

        # Forward CL arguments
        print(str(len(arg)) + ' CL arguments found: ' + str(arg))
        print(sys.argv[0])
        codeVersion = sys.argv[1]
        logFile = sys.argv[2]

        # Format additional arguments
        arguments = ''.join(str(e) for e in sys.argv[3:])
        print('Additional arguments: ', arguments)

    print('Code version:', codeVersion)
    print('Log file:', logFile)

    # Perform calculation in /dev/shm for better IO performance
    # Check if /dev/shm is available on the system
    if os.path.exists('/dev/shm') == False:
        sys.exit('Error! /dev/shm does not exist on this system!')
    timestamp = time.strftime('%Y%m%d%H%M%S')
    tmpfs = '/dev/shm/tmpfs_' + str(timestamp)
#    if os.path.islink(cwd + '/scratch'):
#        sys.exit('Error! Delete previous scratch link!')

    # Information on available memory
    mem = virtual_memory().total/1024/1024/1024
    print('/dev/shm/ is limited to half the total RAM by default. Total ',
          'memory: ' + str(round(mem, 3)) + ' GB.')
    # Copy run directory to tmpfs
    folder_size = 0
    for path, dirs, files in os.walk(cwd):
        for name in files:
            # Ignore possible dangling symlinks
            if os.path.isfile(os.path.join(path, name)):
                folder_size += os.path.getsize(os.path.join(path, name))
    print('Copy run folder (' +
          str(round(folder_size/1024/1024/1024, 3)) + ' GB) to ' + tmpfs)
    time_startcopy = time.monotonic()
    shutil.copytree(cwd, tmpfs, symlinks=True, ignore_dangling_symlinks=True, ignore=shutil.ignore_patterns('results_*'))
    print('Time to copy from HDD to tmpfs: ' +
          str(round(time.monotonic() - time_startcopy, 2)) + ' s.')
    # Create symbolic link for easy access of tmpfs directory
    os.symlink(tmpfs, cwd + '/scratch_' + str(timestamp))

    # Change to tmpfs before executing coupling program
    os.chdir(tmpfs)
    time_startcoupling = time.monotonic()
    if mode == 'external':
        cmd = ('python3 ~/Coupling/code/cplTherm/master.py ' + arguments + ' > ' + os.path.join(tmpfs, logFile) + ' 2>&1')
        print('Execute: ' + cmd)
        subprocess.check_call(cmd, shell=True)
        # subprocess.check_output(["ping", "-c", "1", "8.8.8.8"])
    elif mode == 'interactive':
        # Bad practice! Do not change sys.argv. Just for debugging purpose
        # in order to emulate the script behavior.
        sys.argv.append(arguments) # TODO: Check if append works when arguments contains > 1 entries
        print('sys.argv = ', str(sys.argv))
        
        log_redirect = 'logfile' # 'logfile'
        if log_redirect == 'console':
            import master # master
        elif log_redirect == 'logfile':
            # Redirect stdout to log file
            from contextlib import redirect_stdout
            strng = io.StringIO()
            with redirect_stdout(strng):
                import master # master
            with open(os.path.join(tmpfs, logFile), 'w') as file:
                file.write(strng.getvalue())
        else:
            sys.exit('###\nError: Log ouput must be redirected to either logfile or console.')
    print('Coupling finished after ' +
          str(round(time.monotonic() - time_startcoupling, 2)) + ' s.')

#    # Rsync results from tmpfs to source
#    subprocess.check_output(['rsync', '-a', '--exclude', '"log.wrapper"',
#                             tmpfs + '/', cwd])
    # Change back to cwd
    os.chdir(cwd)

    # Remove tmpfs directory
#    if not preserve_shm:
#        print('Remove tmpfs: ' + tmpfs)
#        shutil.rmtree(tmpfs)

    # Print elapsed time
    print('Wrapper script sucessfully finished after',
          time.time() - time_startwrapper, 's.')

except Exception as ex:
    print(ex)
    # Print elapsed time
    print('Wrapper script crashed after',
          time.time() - time_startwrapper, 's.')
