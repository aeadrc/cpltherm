#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This module reads mesh data and converts results to universal format."""
# Add new code through additional elif entry

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import subprocess
import sys, os, io, re, shutil
import logging
logger = logging.getLogger(__name__)

"""
ccx:    nodes, temperature
        faceCenter, specific heat flux

OF:     faceCenter, temperature
        faceCenter, specific heat flux

CFX:    nodes, temperature
        nodes, specific heat flux

"""

###############################################################################
## Function convert_format
###############################################################################
def convert_format(code, cpl, read_mesh, variable):
    """
    code: fea, cfd
    read_mesh: True, False
    variable: 'temperature', 'heatFlux', 'Robin', 'None'
    """
    import barycenters

    ###########################################################################
    ## Function convert_format: ccx
    ###########################################################################
    if code.name == 'ccx':
        # Reread mesh and element/node/coordinates only if necessary
        if read_mesh == True:
            print(code.name + ': Read mesh data.')

            # Get element - node relation from ccx file all.msh
            file = code.path + '/all.msh'
            if os.path.exists(file) == False:
                sys.exit('Error: ' + file + ' does not exist (Should be '
                         'created by ccx prior to run.')
            matchline = 0
            for num, line in enumerate(open(file)):
                if 'ELEMENT' in line: matchline = num
            # Read (Element, Node1, ..., NodeN) after ELEMENT keyword
            mesh_element_nodes = np.genfromtxt(file, dtype=None,
                                               delimiter=',', comments='*',
                                               skip_header=matchline+1,
                                               autostrip=True)

            # Get nodelist of patch from ccx file patchName.msh
            file = code.path + '/' + code.patch_name + '.msh'
            if os.path.exists(file) == False:
                sys.exit('Error: ' + file + ' does not exist (Should be '
                         'created by ccx prior to run.')
            # Read (Node, x, y, z)
            mesh_nodelist = np.genfromtxt(file,
                                          dtype=[('nodeID', '<i8'),
                                                 ('x', '<f8'),
                                                 ('y', '<f8'),
                                                 ('z', '<f8')],
                                          delimiter=',', comments='*',
                                          skip_header=0, autostrip=True)

            # Get surfacelist of patch from ccx file patchName.sur
            file = code.path + '/' + code.patch_name + '.sur'
            if os.path.exists(file) == False:
                sys.exit('Error: ' + file + ' does not exist (Should be '
                         'created by ccx prior to run.')
            # Read (Element, SX)
            mesh_surfacelist = np.genfromtxt(file,
                                             dtype=[('elementID', np.int32),
                                                    ('surface', 'U20')],
                                             delimiter=',', comments='*',
                                             skip_header=0, autostrip=True)
            # Fix for 1D calculations with only one interface face
            # transform () -> (1, 5)
            if str(np.shape(mesh_surfacelist)) == '()':
                mesh_surfacelist = np.array([mesh_surfacelist])

            logging.debug('mesh_element_nodes: ' +
                          str(np.shape(mesh_element_nodes)))
            logging.debug(str(mesh_element_nodes))
            logging.debug('mesh_nodelist: '+ str(np.shape(mesh_nodelist)))
            logging.debug(str(mesh_nodelist))
            logging.debug('mesh_surfacelist: ' +
                          str(np.shape(mesh_surfacelist)))
            logging.debug(str(mesh_surfacelist))

            # Check if more than one face is assigned to one single element
            if not np.size(mesh_surfacelist['elementID']) == np.size(np.unique(mesh_surfacelist['elementID'])):
                sys.exit('Error: It is assumed that only one face of each '
                         'ccx element lays on the interface. i.e. for convex '
                         'solids with >= 2 faces on the interface the '
                         'barycenter would not be calculated correctly (only '
                         'one barycenter for 2 faces is calculated)')
            else:
                pass
            
            # Compare mesh_element_nodes and mesh_surfacelist to find
            # elements that are adjacent to the patch (mesh_surfacelist) and
            # the corresponding nodes (mesh_element_nodes)
            logging.debug('Determine elements adjacent to patch:')

            # Return indices that would sort all elements in all.msh (ascending)
            sort = np.argsort(mesh_element_nodes[:, 0])

            # Find the indices in the sorted(!) array mesh_element_nodes[:, 0] 
            # (is sorted by sorter=sort) such that, if the corresponding 
            # elements in mesh_surfacelist['elementID'] were inserted before 
            # the indices, the order of a would be preserved.
            # i.e. "rank" contains only the indices of elements adjacent to the
            # patch surface (face)
            rank = np.searchsorted(mesh_element_nodes[:, 0],
                                   mesh_surfacelist['elementID'],
                                   sorter=sort) 
            elementindex = sort[rank] # Should be redundant to simply "rank"
            logging.debug('Index of elements in mesh_element_nodes that are '
                          'adjacent to patch (in order as they appear in '
                          'mesh_surfacelist:')
            logging.debug(str(elementindex))
            logging.debug('Patch adjacent elements plus their nodes:')
            logging.debug(str(mesh_element_nodes[elementindex, :]))

            barycenter_vector_xyz = np.empty((len(elementindex), 3),
                                             dtype=np.float)
            barycenter_vector = np.empty((len(elementindex), 1),
                                         dtype=[('elementID', np.int32),
                                                ('surface', 'U20'),
                                                ('x', '<f8'), ('y', '<f8'),
                                                ('z', '<f8')])
            # Test if there are enough vertices on patch to form face
            test_if_face = True
            for i in range(len(elementindex)):
                logging.debug(i)
                logging.debug(str(mesh_element_nodes[elementindex[i], 1:]))
                logging.debug('Determine which vertices of this element are on'
                              ' the patch:')

                _, nodeindex = np.where(np.array(mesh_nodelist['nodeID'])
                                        == np.array(mesh_element_nodes
                                                    [elementindex[i], 1:])
                                        [:, None])
                logging.debug(str(mesh_nodelist['nodeID'][nodeindex]))
                logging.debug('Print vertex coordinates (x, y, z):')
                logging.debug(str(mesh_nodelist['x'][nodeindex]))
                logging.debug(str(mesh_nodelist['y'][nodeindex]))
                logging.debug(str(mesh_nodelist['z'][nodeindex]))
                logging.debug('Diese Koordinaten müssen nun an barycenter '
                              'übergeben werden.')

                # Check if there are enough nodes (<3) on patch to form a face
                # Eventuell noch checken, ob 1D
                if len(nodeindex) > 2:
                    xyz = np.transpose(
                        np.array((mesh_nodelist['x'][nodeindex],
                                  mesh_nodelist['y'][nodeindex],
                                  mesh_nodelist['z'][nodeindex])))
                    logging.debug(str(xyz))
                    barycenter = barycenters.calc_barycenter(xyz)
                    logging.debug('Barycenter:')
                    logging.debug(str(barycenter))
                else:
                    logging.debug('Too few vertices of element on patch to '
                                  'form a surface.')
                    test_if_face = False
                    barycenter = np.array([np.nan, np.nan, np.nan]) # dummy

                # Complete reconstruction of (elementID, surface, x, y, z),
                # check if this is necessary or if correctly ordered xyz are
                # sufficient
                # elementindex[i] for same order as in mesh_surfacelist
                barycenter_vector['elementID'][i] = mesh_element_nodes[
                    elementindex[i], 0]
                barycenter_vector['surface'][i] = mesh_surfacelist['surface'][i]
                # Note: [i], not elementindex[i]!
                barycenter_vector['x'][i] = barycenter[0]
                barycenter_vector['y'][i] = barycenter[1]
                barycenter_vector['z'][i] = barycenter[2]
                # x, y, z of barycenters, ordered according to
                # mesh_surfacelist['surface']
                barycenter_vector_xyz[i, :] = barycenter

            # Remove lines in array where nodes do not form a face (nan)
            if test_if_face == False:
                barycenter_vector = barycenter_vector[~np.any(
                    np.isnan(barycenter_vector['x']), axis=1)]
                barycenter_vector_xyz = barycenter_vector_xyz[~np.any(
                    np.isnan(barycenter_vector_xyz), axis=1)]

            vertices_vector = np.copy(mesh_nodelist) #['nodeID', 'x', 'y', 'z']
            vertices_vector_xyz = np.transpose(np.array((vertices_vector['x'],
                                                         vertices_vector['y'],
                                                         vertices_vector['z'])))

            logging.debug('barycenter_vector:')
            logging.debug(str(barycenter_vector))
            logging.debug('barycenter_vector_xyz:')
            logging.debug(str(barycenter_vector_xyz))
            logging.debug('vertices_vector:')
            logging.debug(str(vertices_vector))
            logging.debug('vertices_vector_xyz:')
            logging.debug(str(vertices_vector_xyz))

            # Save as object of fea class
            code.barycenter_vector = np.copy(barycenter_vector)
            code.barycenterVectorXYZ = np.copy(barycenter_vector_xyz)
            code.vertices_vector = np.copy(vertices_vector)
            code.vertices_vector_xyz = np.copy(vertices_vector_xyz)
        else:
            pass # Skip reading/initializing mesh

        # CCX: Get temperature at nodes from result file
        if variable == 'temperature':
            # Get temperature results from fea.(re)startFile.dat
            file = code.path + '/' + code.restart_file + '.dat'
            if os.path.exists(file):
                print(file + ' exists.')
            elif not os.path.exists(file) and os.path.exists(code.path + '/'
                                                             + code.start_file
                                                             + '.dat'):
                file = code.path + '/' + code.start_file + '.dat'
                print(file + ' exists.')
            else:
                sys.exit('Error: ' + file + ' does not exist (Should be '
                         'created by ccx prior to run.')

            # Read (Node, T)
            # Find latest occurrence of 'temperatures for set ...'
            matchline = 0
            number_of_rows = np.size(code.vertices_vector['nodeID'])
            for num, line in enumerate(open(file)):
                if 'temperatures for set N' + code.patch_name in line:
                    matchline = num # Will be overwritten until last occurrence
                    grepline = line # check if T is set for correct timestep
            if matchline == 0:
                sys.exit('Error: Result file ' + file 
                         + ' does not contain patch ' + code.patch_name)

            logging.debug(str(code.name) + ': Read results ' + str(grepline))

            nodal_temperature = np.genfromtxt(file,
                                              dtype=[('nodeID', '<i8'),
                                                     ('temperature', '<f8')],
                                              skip_header=matchline+1,
                                              max_rows=number_of_rows,
                                              autostrip=True)
            logging.debug(str(nodal_temperature))

            # Check if order of nodes is the same as in vertices_vector
            _, nodeindex = np.where(np.array(nodal_temperature['nodeID'])
                                    == np.array(code.vertices_vector['nodeID'])
                                    [:, None])
            nodal_temperature_xyz = np.column_stack(
                [code.vertices_vector_xyz,
                 np.transpose(nodal_temperature['temperature'][nodeindex])])
            logging.debug(str(nodeindex))
            logging.debug(str(nodal_temperature_xyz))

            # Save as object of fea class
            code.output = np.copy(nodal_temperature_xyz)

        elif variable == 'heatFlux':
            print(code.name + ': Variable not implemented!')
            sys.exit()
        elif variable == 'None':
            pass
        else:
            sys.exit(code.name + ': Variable not implemented!')

    ###########################################################################
    ## Function convert_format: OpenFOAM version 6
    ###########################################################################
    elif code.name == 'OF6':
        if read_mesh == True: # Reread mesh only if necessary
            # The next OF post processing file is used to get the face centers
            textfile = (code.path + '/system/faceCentres')
            print(code.name + ': Write ' + textfile)
            try:
                file = open(textfile, 'w')
                # Note: sample p (or any other dummy) as it is always available
                # from the start. The T boundary condition needs initialization
                # that is not available yet
                strng = ('#includeEtc "caseDicts/postProcessing/visualization/'
                         'surfaces.cfg"\n'
                         'fields       ( p );\n'
                         '// Override settings here, e.g.\n'
                         'region          FLUID;\n'
                         'interpolationScheme cellPointFace;\n'
                         'surfaceFormat   foam;\n'
                         'writeControl    timeStep;\n'
                         'writeInterval   1;\n'
                         'surfaces\n'
                         '(\n'
                         '    ' + str(code.patch_name) + '\n'
                         '    {\n'
                         '        $patchSurface;\n'
                         '        patches     ( '+ str(code.patch_name) +' );\n'
                         '        interpolate false;\n'
                         '        triangulate false;\n'
                         '    }\n'
                         ');\n')
                file.write(strng)
                file.close()
            except:
                sys.exit('Error: Cannot write file ' + textfile)

            # Change to directory where commands have to be executed
            os.chdir(code.path)
            # Postprocess in order to get faceCentres
            cmd = ('postProcess -func faceCentres -time '
                   + code.start_from + ' -region FLUID > '
                   + code.path + '/log.faceCentres 2>&1')
            print(code.name + ': ' + cmd)
            subprocess.check_call(cmd, shell=True)
            # Change back to coupling directory
            os.chdir(cpl.path)

            # TODO: Check if we need a fix for 1d output

            # Read faceCentres
            # TODO: Make time variable
            file = (code.path + '/postProcessing/faceCentres/0/'
                    + str(code.patch_name) +'/faceCentres')
            if os.path.exists(file) == False:
                sys.exit('Error: ' + file + ' does not exist.')

            # Check if OpenFOAM output format of faceCentres needs a correction.
            with open(file) as f:
                count = sum(1 for _ in f)
            if count == 1:
                logging.debug('Correct wrong OpenFOAM output of faceCentres (all entries in one line without newlines).')
                clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'\n(').replace(b'))', b')\n)'))  # Introduce newlines.
                with open(file, 'wb') as fd:
                    fd.write(b'\n')                     # Empty first line
                    fd.write(clean_lines.getvalue())    # Write corrected format

            # Delete parenthesis
            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'')
                                     .replace(b')', b''))
            # Get no of faces
            n_faces = np.genfromtxt(file, dtype=int, skip_header=1,
                                    max_rows=1, autostrip=True)
            barycenter_vector = np.genfromtxt(clean_lines,
                                              dtype=[('x', '<f8'),
                                                     ('y', '<f8'),
                                                     ('z', '<f8')],
                                              delimiter=' ',
                                              skip_header=3,
                                              max_rows=n_faces,
                                              autostrip=True)

            # Save as object of cfd class
            code.barycenter_vector = np.copy(barycenter_vector)
            logging.debug(np.shape(code.barycenter_vector))

            # (Re)Initialize code.output
            code.output = np.empty((n_faces, 6), dtype=np.float)
            logging.debug(np.shape(code.output))
            for i in range(n_faces):
                code.output[i, 0] = code.barycenter_vector['x'][i]
                code.output[i, 1] = code.barycenter_vector['y'][i]
                code.output[i, 2] = code.barycenter_vector['z'][i]

            # Copy to interface_in
            os.mkdir(code.path + '/interface_in')
            shutil.copy2(code.path + '/postProcessing/faceCentres/'
                            + code.start_from + '/' + code.patch_name
                            + '/faceCentres', code.path + '/interface_in/faceCentres')


        ## Read OpenFOAM output ###############################################
        # if clause needed because there is also the dummy 'None' that skips the next part.
        if variable == 'heatFlux' or variable == 'Robin' or variable == "temperature":
            # Assume/Ensure that both wallHeatFlux and T are always written by OpenFOAM
    
            ## Determine latest time directory
            print(code.name + ': Search time directories in ' + code.path
                  + '/postProcessing/interface_out')
            try:
                dirlist = os.listdir(code.path
                                     + '/postProcessing/interface_out')
            except:
                sys.exit('Error: ' + code.path + '/postProcessing/interface_out '
                         'does not exist.')
            # Loop through all dirList entries
            for i in range(len(dirlist)):
                try:
                    # Check if folder can be converted to float
                    np.array(dirlist)[i].astype(float)
                except ValueError:
                    dirlist[i] = '-1' # Set to -1 otherwise
            # Sort ascending to ...
            sort = np.argsort(np.array(dirlist).astype(float))
            # ... get latest time_dir
            time_dir = dirlist[sort[-1]]
            print(code.name + ': Latest time directory found: ' + time_dir)
    
    
            ## Get wallHeatFlux from OpenFOAM postProcessing
            file = (code.path + '/postProcessing/interface_out/' + time_dir
                    + '/' + code.patch_name + '/scalarField/wallHeatFlux')
            if os.path.exists(file):
                # Check if OpenFOAM output format of wallHeatFlux needs a correction.
                with open(file) as fd:
                    count = sum(1 for _ in fd)
                if count == 1:
                    with open(file, 'rb') as file_to_check:
                        test_string = file_to_check.read()
                        # Check if corrupted OpenFOAM output T needs a correction.
                        # Sometimes the list of values is reduced to one value within {value} if all faces have the same value.
                        wallHeatFlux_value = re.findall(b'\{(.*?)\}', test_string, re.DOTALL) # Find value enclosed by {value}
                        logging.debug('Correct wrong OpenFOAM output format of scalarField/wallHeatFlux (all entries in one line without newlines).')
                        if wallHeatFlux_value: # if list is not empty, i.e. we have found a pattern in {value}.
                            logging.debug('Expand truncated OpenFOAM output of scalarField/wallHeatFlux from {value} to (value1 value2 ...)')
                            n_faces_for_repetition = int(re.findall(b'(.*?)\{', test_string, re.DOTALL)[0]) # Find leading value X{
                            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'{', b'\n(\n').replace(b'}', (b'\n' + wallHeatFlux_value[0]) * (n_faces_for_repetition-1) + b'\n)').replace(b' ', b'\n'))
                        else:
                            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'\n(\n').replace(b')', b'\n)').replace(b' ', b'\n'))
                    with open(file, 'wb') as fd:
                        fd.write(b'\n')                     # Empty first line
                        fd.write(clean_lines.getvalue())    # Write corrected format
                # Get no of faces
                n_faces = np.genfromtxt(file, dtype=int, skip_header=1,
                                        max_rows=1, autostrip=True)
                # Read wallHeatFlux
                wallHeatFlux = np.genfromtxt(file, dtype=float, comments='#',
                                             skip_header=3, max_rows=n_faces,
                                             autostrip=True)
            else:
                sys.exit('Error: ' + file + ' does not exist.')

    
            ## Get wallTemperature from OpenFOAM postProcessing
            file = (code.path + '/postProcessing/interface_out/' + time_dir
                    + '/' + code.patch_name + '/scalarField/T')
            if os.path.exists(file):
                # Check if OpenFOAM output format of temperature needs a correction.
                with open(file) as fd:
                    count = sum(1 for _ in fd)
                if count == 1:
                    with open(file, 'rb') as file_to_check:
                        test_string = file_to_check.read()
                        # Check if corrupted OpenFOAM output T needs a correction.
                        # Sometimes the list of values is reduced to one value within {value} if all faces have the same value.
                        wallTemperature_value = re.findall(b'\{(.*?)\}', test_string, re.DOTALL) # Find value enclosed by {value}
                        logging.debug('Correct wrong OpenFOAM output format of scalarField/T (all entries in one line without newlines).')
                        if wallTemperature_value: # if list is not empty, i.e. we have found a pattern in {value}.
                            logging.debug('Expand truncated OpenFOAM output of scalarField/T from {value} to (value1 value2 ...)')
                            n_faces_for_repetition = int(re.findall(b'(.*?)\{', test_string, re.DOTALL)[0]) # Find leading value X{
                            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'{', b'\n(\n').replace(b'}', (b'\n' + wallTemperature_value[0]) * (n_faces_for_repetition-1) + b'\n)').replace(b' ', b'\n'))
                        else:
                            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'\n(\n').replace(b')', b'\n)').replace(b' ', b'\n'))
                    with open(file, 'wb') as fd:
                        fd.write(b'\n')                     # Empty first line
                        fd.write(clean_lines.getvalue())    # Write corrected format
                # Get no of faces
                n_faces = np.genfromtxt(file, dtype=int, skip_header=1,
                                        max_rows=1, autostrip=True)
                # Read wallTemperature
                wallTemperature = np.genfromtxt(file, dtype=float, comments='#',
                                            skip_header=3, max_rows=n_faces,
                                            autostrip=True)
            else:
                sys.exit('Error: ' + file + ' does not exist.')

            # Check that OpenFOAM wallHeatFlux and wallTemperature have same length
            if not len(wallTemperature) == len(wallHeatFlux): 
                sys.exit('Error. OpenFOAM wallHeatFlux and wallTemperature have different length.')


            ## Get near-wall refTemperature from OpenFOAM postProcessing
            file = (code.path + '/postProcessing/interface_out/' + time_dir
                            + '/' + code.patch_name + '/scalarField/TNear')
            if os.path.exists(file):
                # Check if OpenFOAM output format of reference (near-wall) temperature needs a correction.
                with open(file) as fd:
                    count = sum(1 for _ in fd)
                if count == 1:
                    with open(file, 'rb') as file_to_check:
                        test_string = file_to_check.read()
                        # Check if corrupted OpenFOAM output TNear needs a correction.
                        # Sometimes the list of values is reduced to one value within {value} if all faces have the same value.
                        refTemperature_value = re.findall(b'\{(.*?)\}', test_string, re.DOTALL) # Find value enclosed by {value}
                        logging.debug('Correct wrong OpenFOAM output format of scalarField/TNear (all entries in one line without newlines).')
                        if refTemperature_value: # if list is not empty, i.e. we have found a pattern in {value}.
                            logging.debug('Expand truncated OpenFOAM output of scalarField/TNear from {value} to (value1 value2 ...)')
                            n_faces_for_repetition = int(re.findall(b'(.*?)\{', test_string, re.DOTALL)[0]) # Find leading value X{
                            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'{', b'\n(\n').replace(b'}', (b'\n' + refTemperature_value[0]) * (n_faces_for_repetition-1) + b'\n)').replace(b' ', b'\n'))
                        else:
                            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'\n(\n').replace(b')', b'\n)').replace(b' ', b'\n'))
                    with open(file, 'wb') as fd:
                        fd.write(b'\n')                     # Empty first line
                        fd.write(clean_lines.getvalue())    # Write corrected format
                # Get no of faces
                n_faces = np.genfromtxt(file, dtype=int, skip_header=1,
                                        max_rows=1, autostrip=True)
                # Read wallTemperature
                refTemperature = np.genfromtxt(file, dtype=float, comments='#',
                                            skip_header=3, max_rows=n_faces,
                                            autostrip=True)
            else:
                logging.info('Cannot read OF TNear. Continue anyway as it is not always needed, e.g. for Dirichlet, Neumann, Robin (with constant alpha).')

    
            # Now operations that depend on the output variable
            if variable == 'heatFlux':
                # Update code.output
                for i in range(n_faces):
                    code.output[i, 3] = np.multiply(wallHeatFlux[i], -1)
            if variable == 'temperature':
                # Update code.output
                for i in range(n_faces):
                    code.output[i, 3] = np.multiply(wallTemperature[i], 1)
            elif variable == 'Robin':
                if code.neighbor_code.variable_h_coeff == True:
                    sys.exit('Under construction: new h_coeff (with neighbor_code) assignment not implemented for time-variable htc')
    #                     code.h_coeff = np.absolute(np.divide(np.multiply(wallHeatFlux, -1), wallTemperature - 290))
    #                     for i in range(n_faces):
    #                         if code.h_coeff[i] < 1000 and code.h_coeff[i] > 100:
    #                             code.output[i, 4] = code.h_coeff[i]
    #                         elif code.h_coeff[i] >= 1000:
    #                             code.output[i, 4] = 1000
    #                         elif code.h_coeff[i] <= 100:
    #                             code.output[i, 4] = 100
    
    #                     code.h_coeff = np.add(np.absolute(900/0.015 * (code.output[:, 1] - 0.015)), 100)
    #                     code.h_coeff = np.add(np.absolute(0/0.015 * (code.output[:, 1] - 0.015)), 1000)
    
                    # Calculate htc based on near wall temperature
                    code.h_coeff = np.absolute(np.divide(np.multiply(wallHeatFlux, 1), wallTemperature - refTemperature))
    #                     code.h_coeff = np.divide(np.multiply(wallHeatFlux, 1), wallTemperature - refTemperature)
                    reference_temperature = np.array(refTemperature)
    
                    # Update code.output
                    min_h_coeff = 1e-6  # limiter of htc
                    max_h_coeff = 1e6 # limiter of htc
                    for i in range(n_faces):
    #                         code.output[i, 4] = code.h_coeff[i]
                        code.output[i, 5] = np.multiply(wallHeatFlux[i], -1)
                        code.output[i, 3] = reference_temperature[i]
                        if code.h_coeff[i] < max_h_coeff and code.h_coeff[i] > min_h_coeff:
                            code.output[i, 4] = code.h_coeff[i]
                        elif code.h_coeff[i] >= max_h_coeff:
                            logging.warning('max_h_coeff artificially limited.')
                            code.output[i, 4] = max_h_coeff
                        elif code.h_coeff[i] <= min_h_coeff:
                            logging.warning('min_h_coeff artificially limited.')
                            code.output[i, 4] = min_h_coeff
    
                elif code.neighbor_code.variable_h_coeff == False:
                    # Ensure that wallHeatFlux and wallTemperature are given at the same points. Otherwise reference_temperature cannot be calculated.
                    reference_temperature = np.add(np.divide(np.multiply(
                        wallHeatFlux, -1), code.neighbor_code.h_coeff), wallTemperature)
                    for i in range(n_faces):
    #                         code.output[i, 4] = code.h_coeff[i]
                        code.output[i, 5] = np.multiply(wallHeatFlux[i], -1)
                        code.output[i, 4] = code.neighbor_code.h_coeff
                        code.output[i, 3] = reference_temperature[i]
    
            # Reread faceCentres
            # TODO: Remove this duplicate code part (already executed at readMesh==True)
            # Necessary for parallel OF calculation since the runTime postProcess
            # utility with "foam" writeFormat orders the faceCenters different
            # from the single core output -> we need to reread x,y,z in order
            # to match the values
            file = (code.path + '/postProcessing/interface_out/' + time_dir
                    + '/' + code.patch_name + '/faceCentres')
            if not os.path.exists(file):
                sys.exit('Error: ' + file + ' does not exist.')

            # Check if OpenFOAM output format of faceCentres needs a correction.
            with open(file) as f:
                count = sum(1 for _ in f)
            if count == 1:
                logging.debug('Correct wrong OpenFOAM output of faceCentres (all entries in one line without newlines).')
                clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'\n(').replace(b'))', b')\n)'))  # Introduce newlines.
                with open(file, 'wb') as fd:
                    fd.write(b'\n')                     # Empty first line
                    fd.write(clean_lines.getvalue())    # Write corrected format
    
            # Delete parenthesis
            clean_lines = io.BytesIO(open(file, 'rb').read().replace(b'(', b'')
                                     .replace(b')', b''))
            # Get no of faces
            n_faces = np.genfromtxt(file, dtype=int, skip_header=1,
                                    max_rows=1, autostrip=True)
            barycenter_vector = np.genfromtxt(clean_lines,
                                              dtype=[('x', '<f8'),
                                                     ('y', '<f8'),
                                                     ('z', '<f8')],
                                              delimiter=' ',
                                              skip_header=3,
                                              max_rows=n_faces,
                                              autostrip=True)
    
            # Save as object of cfd class
            code.barycenter_vector = np.copy(barycenter_vector)
            logging.debug(np.shape(code.barycenter_vector))
    
            # (Re)Initialize code.output
            for i in range(n_faces):
                code.output[i, 0] = code.barycenter_vector['x'][i]
                code.output[i, 1] = code.barycenter_vector['y'][i]
                code.output[i, 2] = code.barycenter_vector['z'][i]

#             # Remove timeDirs of this coupling step if
#             # cpl.final_corrector_step = False
#             if cpl.final_corrector_step == False:
#                 logging.debug('Final corrector step not reached yet. '
#                               'Move newly created timeDirs.')
#                 logging.debug('PROTOTYPE: Careful, this will cause problems '
#                               'if more than 9 nOuterCorrectors are used or '
#                               'writeInterval is smaller than 1.')
#                 count = 1
#                 logging.debug(str(float(dirlist[sort[-count]])))
#                 logging.debug(str(float(code.start_from)))
#                 if logger.getEffectiveLevel() == 10: # Debug mode
#                     while(count <= len(dirlist) and float(dirlist[sort[-count]])
#                           > float(code.start_from)):
#                         #print(str(float(dirlist[sort[-count]])))
#                         #print(str(float(code.startFrom)))
#                         time_dir = dirlist[sort[-count]]
#                         #print(time_dir)
#                         shutil.move(code.path + '/postProcessing/wallHeatFlux/'
#                                     + time_dir, code.path
#                                     + '/postProcessing/wallHeatFlux/corr'
#                                     + str(cpl.corrector_step) + '.' + time_dir)
#                         count += 1
#                 #else: Do nothing, as postProcessing directories will be
#                 # overwritten in the next loop
#    
#             # Fix for 1D with only one interface face (transform (4,) -> (1, 4)
#             if str(np.shape(wallHeatFlux)) == '(4,)':
#                 wallHeatFlux = np.array(wallHeatFlux[None, :])
#             logging.debug('np.shape(wallHeatFlux) '
#                           + str(np.shape(wallHeatFlux)))
                
        elif variable == 'None':
            pass
        else:
            sys.exit('Wrong variable definition.')


    ############################################################################
    ## Function convert_format: CFX
    ############################################################################
    elif code.name == 'CFX18':
        # Reread mesh and element/node/coordinates only if necessary
        if read_mesh == True:
            print(code.name + ': Read mesh data.')
            print('PROTOTYPE: For CFX one has to create an output file before '
                  'start')
            # Get node coordinates from CFX output file
            # Alternativ von /heatFlux.csv, da diese Datei sowieso nach der
            # ersten Iteration erstellt wird
            file = code.path + '/heatFlux.csv'
            if os.path.exists(file) == False:
                sys.exit('Error: ' + file + ' does not exist (Should be '
                         'created by CFX prior to run.')
            # Read (x, y, z, T/q)
            mesh_node_coordinates = np.genfromtxt(file,
                                                  dtype=[('x', 'f8'),
                                                         ('y', 'f8'),
                                                         ('z', 'f8'),
                                                         ('value', 'f8')],
                                                  delimiter=',',
                                                  skip_header=9,
                                                  autostrip=True)
            vertices_vector = np.copy(mesh_node_coordinates)
            vertices_vector_xyz = np.transpose(np.array((vertices_vector['x'],
                                                         vertices_vector['y'],
                                                         vertices_vector['z'])))
            code.vertices_vector = np.copy(vertices_vector)
            code.vertices_vector_xyz = np.copy(vertices_vector_xyz)

        if variable == 'heatFlux' or variable == 'Robin':
            # Get wallHeatFlux from CFX Postprocessing
            file = code.path + '/heatFlux.csv'
            if os.path.exists(file) == False:
                sys.exit('Error: ' + file + ' does not exist (Should have been'
                         ' created by CFX output).')
            wallHeatFlux = np.genfromtxt(file, dtype=None, delimiter=',',
                                         skip_header=9, autostrip=True)
            #ACHTUNG: EVENTUELL NEGATIVER WERT DAVON
            print('TODO: ACHTUNG: NEGATIVER WERT DAVON!')
            wallHeatFlux[:, 3] = np.multiply(wallHeatFlux[:, 3], -1)
            code.output = np.copy(wallHeatFlux)
            logging.debug('code.output, wallHeatFlux, size'
                          + str(np.alen(code.output)) + '\n'
                          + str(code.output))
            if variable == 'Robin':
                # Get temperature from CFX Postprocessing
                file = code.path + '/temperature.csv'
                print('PROTOTYPE: Checken, ob temperature.csv und '
                      'heatFlux.csv bei paralleler und sequentieller '
                      'Kopplung immer zur gleichen Zeit vorliegen!')
                if os.path.exists(file) == False:
                    sys.exit('Error: ' + file + ' does not exist (Should have '
                             'been created by CFX output).')
                wallTemperature = np.genfromtxt(file, dtype=None,
                                                delimiter=',', skip_header=9,
                                                autostrip=True)
                #ACHTUNG: EVENTUELL NEGATIVER WERT DAVON
                wallTemperature[:, 3] = np.multiply(wallTemperature[:, 3], 1)
                logging.debug('wallTemperature, size'
                              + str(np.alen(wallTemperature)) + '\n'
                              + str(wallTemperature))
                logging.warning('TODO: It is assumed that wallTemperature and '
                                'wallHeatFlux are written at the same points!')
                reference_temperature = np.copy(wallTemperature)
                reference_temperature[:, 3] = np.add(
                    np.divide(wallHeatFlux[:, 3], code.h_coeff),
                    wallTemperature[:, 3])
                code.output = np.copy(reference_temperature)

                logging.debug('code.output, reference_temperature'
                              + str(code.output))
        elif variable == 'None':
            pass
        else:
            sys.exit(code.name + ': Variable ' + variable + ' not implemented!')

    else:
        print('Error: Mesh / result format for ' + code.name
              + ' not yet implemented.')

    return None



























################################################################################
## Function write_format
################################################################################
def write_format(code, init_field, variable):
    """
    code: fea, cfd
    init_field: True, False
    variable: 'temperature', 'heatFlux', 'Robin', 'None'
    """

    ############################################################################
    ## Function write_format: ccx
    ############################################################################
    if code.name == 'ccx':
        # Write input for next calculation
        if variable == 'heatFlux':
            print(code.name + ': Write heatFlux to patch ' + code.patch_name)
            if init_field == True:
                print('ccx: Initialize patch with value: ')
                if code.restart == False:
                    init_values_vector = np.multiply(
                        np.ones((len(code.barycenter_vector['elementID']), 1)),
                        np.float(code.init_value))
                elif code.restart == True:
                    # Read init_values_vector from .lock file
                    textfile = code.path + '/' + code.patch_name + '.dfl.lock'
                    # TODO: Add exception handling if .lock file not found
                    logging.error('TODO: Initial values may be not extracted '
                                  'in correct order and at correct position '
                                  '(barycenters)')
                    read_init_values = np.genfromtxt(textfile,
                                                     dtype=[
                                                         ('elementID', '<i8'),
                                                         ('faceID', 'U20'),
                                                         ('value', '<f8')],
                                                     delimiter=',',
                                                     comments='*',
                                                     skip_header=0,
                                                     autostrip=True)
                    init_values_vector = read_init_values['value']
                    logging.warning('TODO: Validate restart initialization '
                                    'for heatFlux boundary condition!')

                # Initialize fea.input (necessary for interpolation that
                # needs correct (x, y, z)Target
                logging.warning('TODO: Check if fea.input initialization is '
                                'correct at this point.')
                code.input = np.column_stack([code.barycenter_vector['x'],
                                              code.barycenter_vector['y'],
                                              code.barycenter_vector['z'],
                                              init_values_vector])

                logging.debug(str(code.input))

                # TODO: Maybe not necessary any more
                code.inputPrev = np.copy(code.input)
            elif init_field == False:
                print(code.name + ': Write patch values')
                logging.debug('fea.input should have been updated by '
                              'interpolation function, so there is nothing '
                              'else to do.')
                logging.debug(code.input)
            else:
                sys.exit('Error: init_field argument not set!')

            # Loop over nInnerSteps for transient ccx boundary
            # Right end of range excludes value
            for n_step in range(1, code.n_inner_steps+1):
                # Write content to patch.dfl
                if n_step == code.n_inner_steps:
                    # Last step
                    textfile = code.path + '/' + code.patch_name + '.dfl'
                else:
                    # Intermediate steps
                    textfile = (code.path + '/' + code.patch_name + '.dfl.'
                                + str(n_step))
                logging.debug('ccx: Write ' + variable + ' to ' + textfile)
                try:
                    logging.debug('code.inputPrev[0, 3]')
                    logging.debug(str(code.inputPrev[0, 3]))
                    logging.debug('code.input[0, 3]')
                    logging.debug(str(code.input[0, 3]))
                    logging.debug('n_step = ' + str(n_step) + ', nInnerSteps = '
                                  + str(code.n_inner_steps))
                    logging.debug(str(np.interp(n_step, [0, code.n_inner_steps],
                                                [code.inputPrev[0, 3],
                                                 code.input[0, 3]])))
                    file = open(textfile, 'w')
                    file.write('** DFlux based on ' + code.patch_name + '\n')
                    for i in range(len(code.barycenter_vector['elementID'])):
                        if n_step == code.n_inner_steps:
                            # n_step == code.nInnerSteps: True for last n_step
                            # OR if ccx.transientBC = False OR True for first
                            # corrector_step in new Coupling interval
                            file.write('%d, %s, %.8e\n' % (np.asarray(code.barycenter_vector['elementID'][i]), ' '.join(map(str, code.barycenter_vector['surface'][i])), code.input[i, 3]))
                        else:
                            file.write('%d, %s, %.8e\n' % (np.asarray(code.barycenter_vector['elementID'][i]), ' '.join(map(str, code.barycenter_vector['surface'][i])), np.interp(n_step, [0, code.n_inner_steps], [code.inputPrev[i, 3], code.input[i, 3]])))
                except:
                    raise #sys.exit('Error: Cannot write file ' + textfile)
                finally:
                    file.close()

            # Write steps.inp that includes separate steps with transient BC
            # PROTOTYPE: Fix coded transientBC (Temperature of patch included)
            with open(code.path + '/steps.inp', 'w') as file:
                # Write empty file in any case (even if n_step == code.nInnerSteps)
                file.write('')
                # Skip last step as it is included in wrapper .inp file.
                for n_step in range(1, code.n_inner_steps):
                    file.write('*STEP, INC=1000000\n*HEAT TRANSFER, DIRECT\n'
                               + str(code.delta_t) + ', ' + str(code.delta_t)
                               + '\n*DFLUX\n*include, input=' + code.patch_name
                               + '.dfl.' + str(n_step) + '\n*NODE PRINT, NSET=N'
                               + code.patch_name + '\nNT\n*END STEP\n\n')

        elif variable == 'Robin':
            print(code.name + ': Write film boundary to patch '
                  + code.patch_name)
            textfile = code.path + '/' + code.patch_name + '.flm'
            if init_field == True:
                print('ccx: Initialize patch with value: ')
                if code.restart == False:
                    init_values_vector = np.multiply(np.ones((len(
                        code.barycenter_vector['elementID']), 1)),
                                                     np.float(code.init_value))
                    init_h_coeff_vector = np.multiply(np.ones((len(
                        code.barycenter_vector['elementID']), 1)), np.float(code.init_h_coeff))
                elif code.restart == True:
                    # Read init_values_vector from .lock file
                    textfile = code.path + '/' + code.patch_name + '.flm.lock'
                    # TODO: Add exception handling if .lock file not found
                    logging.error('TODO: Initial values may be not extracted '
                                  'in correct order and at correct position '
                                  '(barycenters)')
                    read_init_values = np.genfromtxt(textfile,
                                                     dtype=[('elementID',
                                                             '<i8'),
                                                            ('faceID', 'U20'),
                                                            ('value', '<f8'),
                                                            ('htcValue',
                                                             '<f8')],
                                                     delimiter=',',
                                                     comments='*',
                                                     skip_header=0,
                                                     autostrip=True)
                    init_values_vector = read_init_values['value']
                    init_h_coeff_vector = read_init_values['htcValue']
                    logging.info('TODO: Restart initialization for Robin '
                                 'boundary condition only implemented for '
                                 'reference temperature, not for htc!')

                # Initialize fea.input (necessary for interpolation that needs
                # correct (x, y, z)Target
                print('Check if fea.input initialization with init_q_vector = 0'
                      ' is correct here.')
                init_q_vector = np.multiply(np.ones((len(
                    code.barycenter_vector['elementID']), 1)), np.float(0))
                code.input = np.column_stack([code.barycenter_vector['x'],
                                              code.barycenter_vector['y'],
                                              code.barycenter_vector['z'],
                                              init_values_vector,
                                              init_h_coeff_vector,
                                              init_q_vector])
                logging.debug(str(code.input))

                # PROTOTYPE: Maybe not necessary any more
                code.inputPrev = np.copy(code.input)

            elif init_field == False:
                print(code.name + ': Write patch values')
                logging.debug('fea.input has been updated by interpolation '
                              'function (as Tref)')
                logging.debug(str(code.input))
            else:
                sys.exit('Error: init_field argument not set!')

            # Loop over nInnerSteps for transient ccx boundary
            # Right end of range excludes value
            for n_step in range(1, code.n_inner_steps+1):
                # Write content to patch.flm
                if n_step == code.n_inner_steps:
                    # Last step
                    textfile = code.path + '/' + code.patch_name + '.flm'
                else:
                    # Intermediate steps
                    textfile = (code.path + '/' + code.patch_name + '.flm.'
                                + str(n_step))
                logging.debug('ccx: Write ' + variable + ' to ' + textfile)
                try:
                    logging.debug('code.inputPrev[0, 3]')
                    logging.debug(str(code.inputPrev[0, 3]))
                    logging.debug('code.input[0, 3]')
                    logging.debug(str(code.input[0, 3]))
                    logging.debug('code.inputPrev[0, 4]')
                    logging.debug(str(code.inputPrev[0, 4]))
                    logging.debug('code.input[0, 4]')
                    logging.debug(str(code.input[0, 4]))
                    logging.debug('code.inputPrev[0, 5]')
                    logging.debug(str(code.inputPrev[0, 5]))
                    logging.debug('code.input[0, 5]')
                    logging.debug(str(code.input[0, 5]))
                    logging.debug('n_step = ' + str(n_step) + ', nInnerSteps = '
                                  + str(code.n_inner_steps))
                    logging.debug(str(np.interp(n_step, [0, code.n_inner_steps],
                                                [code.inputPrev[0, 3],
                                                 code.input[0, 3]])))
                    file = open(textfile, 'w')
                    file.write('** Film based on ' + code.patch_name + '\n')
                    for i in range(len(code.barycenter_vector['elementID'])):
                        if n_step == code.n_inner_steps:
                            # n_step == code.nInnerSteps: True for last n_step OR
                            # if ccx.transientBC = False OR True for first
                            # corrector_step in new Coupling interval
                            file.write('%d, %s, %.8e, %.8e\n' % (np.asarray(code.barycenter_vector['elementID'][i]), ' '.join(map(str, code.barycenter_vector['surface'][i])), code.input[i, 3], code.input[i, 4]))
                        else:
                            file.write('%d, %s, %.8e, %.8e\n' % (np.asarray(code.barycenter_vector['elementID'][i]), ' '.join(map(str, code.barycenter_vector['surface'][i])), np.interp(n_step, [0, code.n_inner_steps], [code.inputPrev[i, 3], code.input[i, 3]]), np.interp(n_step, [0, code.n_inner_steps], [code.inputPrev[i, 4], code.input[i, 4]])))
                except:
                    raise #sys.exit('Error: Cannot write file ' + textfile)
                finally:
                    file.close()
                # WORKAROUND REPLACE SN by FN
                file = open(textfile, 'r')
                snfn = file.read()
                snfn = re.sub('S', 'F', snfn)
                file.close()
                file = open(textfile, 'w')
                file.write(snfn)
                file.close()

            # Write steps.inp that includes separate steps with transient BC
            # PROTOTYPE: Fix coded transientBC (Temperature of patch included)
            with open(code.path + '/steps.inp', 'w') as file:
                # Write empty file in any case (even if n_step == code.nInnerSteps)
                file.write('')
                # Skip last step as it is included in wrapper .inp file
                for n_step in range(1, code.n_inner_steps):
                    file.write('*STEP, INC=1000000\n*HEAT TRANSFER, DIRECT\n'
                               + str(code.delta_t) + ', ' + str(code.delta_t)
                               + '\n*FILM\n*include, input=' + code.patch_name
                               + '.flm.' + str(n_step) + '\n*NODE PRINT, NSET=N'
                               + code.patch_name + '\nNT\n*END STEP\n\n')
        else:
            sys.exit('write_format for ' + variable + ' not implemented.')

    ###########################################################################
    ## Function write_format: OpenFOAM
    ###########################################################################
    elif code.name == 'OF6':
        if init_field == True:
            # The next OF post processing file is used to write the CFD output
            # on the fly (without starting postProcess utility manually)
            # Make sure it is included in controlDict.functions
            textfile = (code.path + '/system/interface_out')
            print(code.name + ': Write ' + textfile)
            try:
                file = open(textfile, 'w')
                strng = ('#includeEtc "caseDicts/postProcessing/visualization/'
                         'surfaces.cfg"\n'
                         'fields       ( T wallHeatFlux );\n'
                         '// Override settings here, e.g.\n'
                         'region          FLUID;\n'
                         'interpolationScheme cellPointFace;\n'
                         'surfaceFormat   foam;\n'
                         'writeControl    writeTime;\n'
                         'writeInterval   1;\n'
                         'surfaces\n'
                         '(\n'
                         '    ' + str(code.patch_name) + '\n'
                         '    {\n'
                         '        $patchSurface;\n'
                         '        patches     ( '+ str(code.patch_name) +' );\n'
                         '        interpolate false;\n'
                         '        triangulate false;\n'
                         '    }\n'
                         ');\n')
                # Note that TNear was deleted in line 'fields       ( T wallHeatFlux TNear );\n' as it is not used any more and generates a warning in the OF log.
                file.write(strng)
                file.close()
            except:
                sys.exit('Error: Cannot write file ' + textfile)

        if variable == 'temperature':
            print(code.name + ': Write temperature to patch '
                  + code.patch_name)
            if init_field == True:
                # Use changeDictionary to impose correct T boundary condition
                # timeVaryingMappedFixedValue on interface
                textfile = (code.path + '/system/FLUID/changeDictionaryDict.'
                            'timeVaryingMappedFixedValue')
                print(code.name + ': Write ' + textfile)
                with open(textfile, 'w') as file:
                    strng = ('''
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      changeDictionaryDict;
}

T
{
    boundaryField
    {
        ''' + str(code.patch_name) + '''
        {
            type            timeVaryingMappedFixedValue;
            setAverage      false;//| Switch to activate setting of average value | no | false
            perturb         0;////| Perturb points for regular geometries | no | 1e-5
            fieldTable      T;//| Alternative field name to sample | no| this field name
            mapMethod       nearest;//| Type of mapping            | no | planarInterpolation
            offset          0;//| Offset to mapped values    | no | Zero
            dataDir         "interface_in";//| Top-level directory of the points and field data \\
                            //    | no | constant/boundaryData/\<patch name\>
            points          "faceCentres";//| Path including name of points file relative to dataDir \\
                            //    | no | points
    //        sample          FLUID_to_SOLID/scalarField;//| Name of the sub-directory in the time directories \\
                            //    containing the fields | no | ""
        }
    }
}''')
                    file.write(strng)

                # Change to directory where commands have to be executed
                os.chdir(code.path)
                # Change boundary condition in startTime directory, so we can
                # execute createExternalCoupledPatchGeometry later
                cmd = ('changeDictionary -time ' + code.start_from
                       + ' -region FLUID -dict'
                       ' system/changeDictionaryDict.timeVaryingMappedFixedValue'
                       ' > ' + code.path + '/log.changeDictionary.'
                       'timeVaryingMappedFixedValue 2>&1')
                print(code.name + ': ' + cmd)
                subprocess.check_call(cmd, shell=True)
                # Change back to coupling directory
                os.chdir('..') # TODO: Absolute path! Cpl not available.


                # Initial value
                print(code.name + ': Initialize patch with value: ')
                init_values_vector = np.multiply(
                    np.ones((len(code.barycenter_vector['x']), 1)),
                    np.float(code.init_value))
                # Initialization cfd.input (necessary for interpolation that
                # needs correct (x, y, z)Target
                print('Check if cfd.input init is correct at this point.')
                code.input = np.column_stack([code.barycenter_vector['x'],
                                              code.barycenter_vector['y'],
                                              code.barycenter_vector['z'],
                                              init_values_vector])
                logging.debug(str(code.input))
            elif init_field == False:
                print(code.name + ': Write patch values')
                logging.debug('cfd.input should have been updated by '
                              'interpolation function, so there is nothing '
                              'else to do.')
                logging.debug(str(code.input))
            else:
                sys.exit('Error: init_field argument not set!')

            # Write timeVaryingMappedFixedValue in correct format
            path = code.path + '/interface_in/' + str(code.start_from)
            if os.path.exists(path) == False:
                os.mkdir(path)
            textfile = path + '/T'
            print(code.name + ': Write ' + variable + ' to ' + textfile)
            try:
                file = open(textfile, 'w')
            except:
                sys.exit('Error: Cannot open file ' + textfile)
            strng = ('\n' + str(len(code.input[:, 0])) + '\n(\n'
                     + '\n'.join(map(str, code.input[:, 3])) + '\n)')
            file.write(strng)
            file.close()

    ############################################################################
    ## Function write_format: CFX
    ############################################################################
    elif code.name == 'CFX18':
        os.chdir(code.path) # Change to dir where commands have to be executed
        if variable == 'temperature':
            if init_field == True:
                textfile = code.path + '/initFlowAnalysis.pre'
                print('PROTOTYPE: ' + code.name + ': Write initialization not '
                      'implemented!!')

                # Do not do this here (code.vertices_vector is created at
                # function convert_format, and this function is executed after
                # initialization)
                print('Try to initialize patch with value: ')
                init_values_vector = np.multiply(
                    np.ones((len(code.vertices_vector['x']), 1)),
                    np.float(code.init_value))
                code.input = np.column_stack([code.vertices_vector['x'],
                                              code.vertices_vector['y'],
                                              code.vertices_vector['z'],
                                              init_values_vector])
                print(code.input)

                try:
                    print(textfile)
                    file = open(textfile, 'r')
                    logging.debug(str(file))
                    strng = file.read()
                    strng = re.sub(r'writeCaseFile.*?write def file',
                                   'writeCaseFile filename=' + code.path + '/'
                                   + code.start_file
                                   + '.def, operation=write def file', strng)
                    strng = re.sub(r'writeCaseFile.*?cfx', 'writeCaseFile '
                                   'filename=' + code.path + '/'
                                   + code.start_file + '.cfx', strng)
                    strng = re.sub(r'File Name.*', 'File Name = ' + code.path
                                   + '/temperature.csv', strng, re.M)
                    file.close()
                    file = open(textfile, 'w')
                    file.write(strng)
                    file.close()
                except:
                    sys.exit('Error: Cannot open file for reading/writing: '
                             + textfile)

#             elif initField == False:
            # Write to temperature.csv
            textfile = code.path + '/temperature.csv'
            print(code.name + ': Write ' + variable + ' to ' + textfile)
            try:
                file = open(textfile, 'w')
            except:
                sys.exit('Error: Cannot write file ' + textfile)

            # Write header
            file.write('\n[Name]\nTProfile\n\n[Spatial Fields]\nx, y, z\n\n'
                       '[Data]\nx [ m ], y [ m ], z [ m ], Temperature [ K ]\n')
            for i in range(len(code.vertices_vector['x'])):
                file.write('%.8e, %.8e, %.8e, %.8e\n' % (
                    code.vertices_vector['x'][i], code.vertices_vector['y'][i],
                    code.vertices_vector['z'][i], code.input[i, 3]))
            file.close()

        else:
            sys.exit('write_format for ' + variable + ' not implemented.')

    else:
        sys.exit('write_format for ' + code.name + ' not implemented.')
    return None

