#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" This module contains controls for FEA/CFD coupling"""
# Add new code through additional elif entry

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import subprocess
import shutil
import numpy as np
import logging
import re

def set_time(position, cpl, code, neighbor, tarball):
    """ This is the function to control start/stop time for FEA/CFD """
    logger = logging.getLogger(__name__)
    round_precision = 12 # Round cpl_interval to avoid 4.9999999999999996e-06 and so on.
    
    if position == 'end_corrector_step':
        if code.name == 'ccx':
            logging.debug('neighbor.converged = ', str(neighbor.converged))
            logging.debug('cpl.corrector_step = ', str(cpl.corrector_step))
            logging.debug('cpl.n_iter = ', str(cpl.n_iter))
            # Assume that CalculiX in transient and OF steady
            
            # User specified limiting values
            dTmax_allowed = cpl.max_interface_temperature_change # Time step criterion based on dTmax
            dtau_min = code.delta_t             # FEA solver time size
            dtau_max = cpl.max_cpl_interval # Max coupling interval size given in inputDeck restricts max time step
                
            if cpl.cpl_interval_dynamic == 'static':
                # No dynamic adjustment and fallback for previous versions
                if (neighbor.converged == True) or (cpl.corrector_step == cpl.n_outer_correctors):
                    cpl.simtime['currentSimTime'] = round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision) # Update simTime at end of coupling interval
                    cpl.user_converged = True # Flag needed for matching end time correctly
                    
                    if cpl.n_iter == 1:
                        dT = code.dict_output[cpl.n_iter][cpl.corrector_step][:,3] - neighbor.init_value
                    elif cpl.n_iter >= 2:
                        dT = code.dict_output[cpl.n_iter][cpl.corrector_step][:,3] - code.dict_output[cpl.n_iter-1][list(code.dict_output[cpl.n_iter-1].keys())[-1]][:,3]
                    dTmax = np.linalg.norm(dT,np.inf)
                    print('Time control: Induced interface temperature change over coupling interval', str(cpl.n_iter), 'is', str(dTmax), 'K.')
                else:
                    pass
            elif (cpl.cpl_interval_dynamic == 'explicit') or (cpl.cpl_interval_dynamic == 'implicit'):
                # Explicit/Implicit modification based on current and previous converged results
                # If coupling interval is converged or max number of iterations is reached, then explicitly set next coupling interval size.
                # Calculate interface temperature dT change between intervals as indicator function
                if (neighbor.converged == True) or (cpl.corrector_step == cpl.n_outer_correctors):
                    if cpl.n_iter == 1:
                        # at the end of the first coupling interval, only the current results and from the init are available.
                        # The cfd.init_value that is given in the inputDeck.inp may be not suited if ccx is restarted from a converged conjugate steady state.
                        # The initial restart values are however not available.
                        dT = code.dict_output[cpl.n_iter][cpl.corrector_step][:,3] - neighbor.init_value
                        
                        # TODO: Check if this is a good idea. Othweise comment in next lines and put the rest to elif cpl.n_iter >= 2
                        # An option would be to set dT = 1e-12. In this case code.cpl_interval = dtau_max is always chosen.
                        dT.fill(1e-12)
                        
#                        # Caclulate max interface temperature change
#                        dTmax = np.linalg.norm(dT,np.inf)
#                        print('Induced interface temperature change between coupling intervals', str(cpl.n_iter-1), 'and', str(cpl.n_iter), 'is', str(dTmax), 'K. Allowed threshold is', str(dTmax_allowed), 'K.')
#                        print('Interval size adjustment is turned off for first coupling interval as initial interface value can be chosen arbitrarily.')

                        # Caclulate max interface temperature change
                        dTmax = np.linalg.norm(dT,np.inf)
    
                        c_adapt = np.floor(code.cpl_interval/dtau_min+0.5) # No scaling for first coupling interval


                    elif cpl.n_iter >= 2:
                        # From second interval on results from current and previous interval are available
                        dT = code.dict_output[cpl.n_iter][cpl.corrector_step][:,3] - code.dict_output[cpl.n_iter-1][list(code.dict_output[cpl.n_iter-1].keys())[-1]][:,3]

                        # Caclulate max interface temperature change
                        dTmax = np.linalg.norm(dT,np.inf)
                        
                        # Calculate scaling factor
                        # cpl.cpl_interval_dynamic_acceleration_factor = 1 by default if not specified otherwise in inputDeck.inp
                        # Distinguish between two options:
                        # 1. Scaling based on dT ratio. May introduce strong changes.
                        # 2. Scaling with constant factor as soon as dT ratio exceeds this factor (.i.e. a limit)
                        
#                        c_adapt = np.floor(dTmax_allowed/dTmax*code.cpl_interval/dtau_min+0.5) # Scaling factor (multiple of dt solver)
                        c_adapt = np.floor(dTmax_allowed/dTmax*code.cpl_interval/dtau_min) # Scaling factor (multiple of dt solver), round always towards 0.
                        
                        if np.abs(cpl.cpl_interval_dynamic_acceleration_factor-1) > 1e-6: # i.e. factor is not 1
                            if ((dTmax_allowed/dTmax) > cpl.cpl_interval_dynamic_acceleration_factor): # limit increase in dtCpl
                                c_adapt = np.floor(cpl.cpl_interval_dynamic_acceleration_factor*code.cpl_interval/dtau_min)
                            elif ((dTmax_allowed/dTmax) < 1): # always scaling with constant factor if dtCpl has to be decreased. 
                                c_adapt = np.floor(1/cpl.cpl_interval_dynamic_acceleration_factor*code.cpl_interval/dtau_min)
                            else:
                                pass # Do not overwrite c_adapt as is is in allowed range
#                            if ((dTmax_allowed/dTmax) > cpl.cpl_interval_dynamic_acceleration_factor):
#                                c_adapt = np.floor(cpl.cpl_interval_dynamic_acceleration_factor*code.cpl_interval/dtau_min)
#                            elif ((dTmax_allowed/dTmax) < 1/cpl.cpl_interval_dynamic_acceleration_factor):
#                                c_adapt = np.floor(1/cpl.cpl_interval_dynamic_acceleration_factor*code.cpl_interval/dtau_min)
#                            else:
#                                pass # Do not overwrite c_adapt as is is in allowed range
                        else:
                            pass # Do nothing as dummy value cpl.cpl_interval_dynamic_acceleration_factor = 1 turns of limits.
                                    

                    print('Time control: Induced interface temperature change over coupling interval', str(cpl.n_iter), 'is', str(dTmax), 'K. Allowed threshold is', str(dTmax_allowed), 'K.')
                    
                    if cpl.cpl_interval_dynamic == 'explicit':
                        # Write SimTime before adjustment for next interval.
                        cpl.simtime['currentSimTime'] = round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision) # Update simTime at end of coupling interval
                        cpl.user_converged = True # Flag needed for matching end time correctly
                        
                        print('Time control: Try to adjust coupling interval size (' ,str(code.cpl_interval) , 's) for next interval number', str(cpl.n_iter+1), '.')
                        if c_adapt <= 1:
                            code.cpl_interval = round(dtau_min,round_precision) # Limit next coupling interval size to solver time step
                            print('Time control: Cannot reduce cpl_interval further than solver time step.')
                        elif c_adapt >= dtau_max/dtau_min:
                            code.cpl_interval = round(dtau_max,round_precision)
                            print('Time control: Cannot increase cpl_interval further than original max_cpl_interval.')
                        else:
#                            code.cpl_interval = c_adapt*dtau_min
                            code.cpl_interval = round(c_adapt*dtau_min,round_precision) # Try to be always a litte below maximal allowed coupling interval size
                        print('Time control:', str(code.name) + '.cpl_interval set to', code.cpl_interval, 's for next interval.')                    
                    elif cpl.cpl_interval_dynamic == 'implicit':
                        if (dTmax > dTmax_allowed) and (cpl.corrector_step < cpl.n_outer_correctors): # Use implicit adjustment only if dT is too large. Else it is a waste of computational time.
                            print('Time control: Try to adjust coupling interval size (' ,str(code.cpl_interval) , 's) for current interval number', str(cpl.n_iter))
                            if code.cpl_interval == dtau_min:
                                print('Time control: Ran twice into minimal coupling interval size limit. Allow to continue even if dTmax > dTmax_allowed')
                                cpl.user_converged = True # If tau is already minimal from previous step then continue (in order to avoid infinity/unnecessary loop if dTmax criterion cannot be matched)
                                # Write SimTime before adjustment for next interval.
                                cpl.simtime['currentSimTime'] = round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision) # Update simTime at end of coupling interval
                            else:
                                cpl.user_converged = False # Delay convergence (implicitly recalculate current coupling interval)
                                cpl.reset_Aitken = True # Reset Aitken relaxation because change of interval size would mess up with dynamic relaxation

                            if c_adapt <= 1:
                                code.cpl_interval = round(dtau_min,round_precision)
                                print('Time control: Cannot reduce cpl_interval further than solver time step.')
                            elif c_adapt >= dtau_max/dtau_min: # Can never occur, as c_adapt is always < 1 in this if clause and the ratio dtau_max/fea.cpl_interval > 1.
                                code.cpl_interval = round(dtau_max,round_precision)
                                print('Time control: Cannot increase cpl_interval further than original max_cpl_interval.')
                            else:
#                                code.cpl_interval = c_adapt*dtau_min
                                code.cpl_interval = round(c_adapt*dtau_min,round_precision) # Try to be always a litte below maximal allowed coupling interval size
                            print('Time control:', str(code.name) + '.cpl_interval set to', code.cpl_interval, 's.')
                        else:
                            # Write SimTime before adjustment for next interval.
                            cpl.simtime['currentSimTime'] = round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision) # Update simTime at end of coupling interval
                            print('Time control: Try to adjust coupling interval size (' ,str(code.cpl_interval) , 's) for next interval number', str(cpl.n_iter+1), '.')
                            if c_adapt <= 1:
                                code.cpl_interval = round(dtau_min,round_precision) # Limit next coupling interval size to solver time step
                                print('Time control: Cannot reduce cpl_interval further than solver time step.')
                            elif c_adapt >= dtau_max/dtau_min:
                                code.cpl_interval = round(dtau_max,round_precision)
                                print('Time control: Cannot increase cpl_interval further than original max_cpl_interval.')
                            else:
#                                code.cpl_interval = c_adapt*dtau_min
                                code.cpl_interval = round(c_adapt*dtau_min,round_precision) # Try to be always a litte below maximal allowed coupling interval size                            print('Time control:', str(code.name) + '.cpl_interval set to', code.cpl_interval, 's.')    
                            print('Time control:', str(code.name) + '.cpl_interval set to', code.cpl_interval, 's for next interval.')
                            cpl.user_converged = True # Allow coupling to proceed with the next interval.                        
                else:
                    pass # cfd.converged not True yet.
            
#            if cpl.user_converged == True:
            # Check if next coupling interval will meet or exceed end time (for static, exlicit and converged implicit)
            # Check if current coupling interval will meet or exceed end time (not converged implicit). Note: here, cpl.simtime['currentSimTime'] is not updated yet, so it will work
            if (round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision) >= cpl.simtime['maxSimTime']):
                print('Time control: The calculated cpl_interval size will exceed the maximum simulation time.')
                print('Time control: currentSimTime =', str(cpl.simtime['currentSimTime']), '; cpl_interval', str(code.cpl_interval), '; maxSimTime =', str(cpl.simtime['maxSimTime']))
                code.cpl_interval = round(cpl.simtime['maxSimTime'] - cpl.simtime['currentSimTime'],round_precision)
                print('Time control: cpl_interval adjusted to ', str(code.cpl_interval), 's.')
            elif (round(cpl.simtime['maxSimTime'] - dtau_min,round_precision) <= round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision)) and (round(cpl.simtime['currentSimTime'] + code.cpl_interval,round_precision) < cpl.simtime['maxSimTime']):
                print('Time control: The calculated cpl_interval size will not allow another min interval size without exceeding the maximum simulation time.')
                print('Time control: currentSimTime =', str(cpl.simtime['currentSimTime']), '; cpl_interval', str(code.cpl_interval), '; dtau_min =', str(dtau_min), '; maxSimTime =', str(cpl.simtime['maxSimTime']))
                code.cpl_interval = round(cpl.simtime['maxSimTime'] - cpl.simtime['currentSimTime'] - dtau_min,round_precision)
                print('Time control: cpl_interval adjusted to ', str(code.cpl_interval), 's.')
            else:
                pass
            


                


    
    elif (position == 'start_corrector_step') or (position == 'pre'):
        print('Set time.')
        logging.info('PROTOTYPE: Use indicator function to adjust delta_t, '
                     'cpl_interval automatically.')
        ###########################################################################
        ## CalculiX
        ###########################################################################
        if code.name == 'ccx':
            # Change cplInterval at specific coupling iteration
            if (cpl.n_iter == code.n_iter_interval_change) and (cpl.corrector_step == 1):
#                print('ccx: Increase cpl_interval from ' + str(code.cpl_interval)
#                      + ' to ' + str(10*code.cpl_interval))
#                code.cpl_interval = 10*code.cpl_interval
#                print('ccx: Increase delta_t from ' + str(code.delta_t) + ' to '
#                      + str(10*code.delta_t))
#                code.delta_t = 10*code.delta_t
                pass # Functionality not needed any more
    
            # cpl.corrector_step > 1: constant BC during first corrector step
            if code.transient_bc == True and cpl.corrector_step > 1:
                # Calculate number of steps during one coupling interval
                code.n_inner_steps = int(np.divide(code.cpl_interval,
                                                   code.delta_t))
            else:
                code.n_inner_steps = 1 # for backwards compatibility
            #code.cplInterval = 100   # Should be adjusted automatically
            #code.deltaT = 1          # Internal time step
            #code.stopAt =           # Not relevant for ccx
            #code.startFrom =        # Always latest?
            #code.writeInterval =    # Not relevant for ccx
    
        ###########################################################################
        ## OpenFOAM
        ###########################################################################
        elif code.name in ['OF4', 'OF6']:
            # Time directories can be written in many formats. In order to address
            # the correct directories we need to search the case directory.
            if code.name == 'OF6':
                try:
                    # For parallel calculation timeDirs are in the processor folders
                    # if they are not reconstructed on the fly.
                    dir_list = os.listdir(code.path + str('/processor0'))
                except FileNotFoundError:
                    dir_list = os.listdir(code.path)
    
            elif code.name == 'OF4':
                # For OF4 timeDirs have to be reconstructed every coupling timestep
                dir_list = os.listdir(code.path)
    
            # Loop through all dir_list entries
            for i in range(len(dir_list)):
                try: # Check if folder can be converted to float
                    np.array(dir_list)[i].astype(float)
                except ValueError: # Set to -1 otherwise
                    dir_list[i] = '-1'
            if not (np.max(np.array(dir_list).astype(float)) > -1):
                # Exit if there are no time directories
                sys.exit(code.name + ': Error: No time directory found.')
            # Sort ascending to ...
            sort = np.argsort(np.array(dir_list).astype(float))
            # ... get latest time_dir
            time_dir = dir_list[sort[-1]]
    
            # print(dir_list)
    
            # Remove / backup timeDirs of this coupling timestep if
            # cpl.finalCorrectorStep = False
            # control is executed before actual run, therefore timeDirs have to be
            # removed only if the previous correction was not final (e.g. at the
            # first corrector of the next coupling timestep)
            
            current_cpl_interval = code.cpl_interval
            
#            use_intermediate_steadyCfdResult_as_init = False
            intermediate_steadyCfdResult_offset = 1
            
            if code.use_intermediate_steadyCfdResult_as_init == False:
                if cpl.corrector_step > 1:
                    if logger.getEffectiveLevel() == 10: # Debug mode
                        logging.debug('Final corrector step not reached yet. Move '
                                      'newly created timeDirs.')
                        logging.debug('PROTOTYPE: Careful, this will cause problems '
                                      'if more than 9 n_outer_correctors are used or '
                                      'write_interval is smaller than 1.')
                        if code.n_procs == 1:
                            # TODO: Add backup for parallel calc (processor0/*)
                            ind = 1
                            while(float(dir_list[sort[-ind]]) > float(code.start_from)):
                                time_dir_mv = dir_list[sort[-ind]]
                                logging.debug('Move time_dir ' + str(time_dir_mv))
                                shutil.move(code.path + '/' + time_dir_mv, code.path + '/'
                                            + 'backup.' + time_dir_mv + '.corr'
                                            + str(cpl.corrector_step-1))
                                ind += 1
                    else:
                        ind = 1
                        while(float(dir_list[sort[-ind]]) > float(code.start_from)):
                            time_dir_mv = dir_list[sort[-ind]]
                            # Remove new timeDirs
                            if code.n_procs == 1:
                                shutil.rmtree(code.path + '/' + time_dir_mv)
                            elif code.n_procs > 1:
                                for n_proc in range(code.n_procs):
                                    shutil.rmtree(code.path + '/processor'
                                                  + str(n_proc) + '/' + time_dir_mv)
                            ind += 1
                    time_dir = code.start_from            
            elif code.use_intermediate_steadyCfdResult_as_init == True:
                if cpl.corrector_step == 1:
                    # Reset label to mark that code.start_from was modified by this recycling of itnermediate results.
                    code.start_from_modified = False
                if cpl.corrector_step > 1:
                    print(code.name + ': Use intermediate steady result as new CFD initialization. Only valid for steady CFD with step size 1.')
                    # Easiest to simply start from 1001, 2001, etc. In this case it's 999 iterations instead of 1000 and the X000er folders are still the final results for postprocessing.
                    if code.start_from_modified == False: # First time I want an offset
                        code.start_from_offset = int(code.start_from)+intermediate_steadyCfdResult_offset
                    else: # From second time on no offset in addition to the first one (otherwise it is X002, X003,...)
                        code.start_from_offset = int(code.start_from)
                    
                    
                    # Remove (alternatively backup) all directories larger than orignial start dir, but not the very last one (which is needed).
                    # Occurs only if writeInterval is such that multiple result dirs are written per coupling interval.
                    ind = 2
                    while(float(dir_list[sort[-ind]]) > float(code.start_from)): # Not 100% true as code.start_from changes.
                        time_dir_mv = dir_list[sort[-ind]]
                        # Remove new timeDirs (but not latest one ind=1)
                        if code.n_procs == 1:
                            shutil.rmtree(code.path + '/' + time_dir_mv)
                        elif code.n_procs > 1:
                            for n_proc in range(code.n_procs):
                                shutil.rmtree(code.path + '/processor'
                                              + str(n_proc) + '/' + time_dir_mv)
                        ind += 1
                    
                    # Rename latest timDir
                    ind = 1
                    time_dir_mv = dir_list[sort[-ind]]
                    if code.n_procs == 1:
                        # Remove existing target folder
                        try:
                            shutil.rmtree(code.path + '/' + str(code.start_from_offset))
                        except:
                            pass
                        # Remove OF time information
                        shutil.rmtree(code.path + '/' + time_dir_mv + '/uniform')
                        # Rename timeDir
                        shutil.move(code.path + '/' + time_dir_mv, code.path + '/' + str(code.start_from_offset)) # Workaround to create 1, 1001, 2001, ... as new starts for corr>1
                    elif code.n_procs > 1:
                        for n_proc in range(code.n_procs):
                            # Remove existing target folder
                            try:
                                shutil.rmtree(code.path + '/processor' + str(n_proc) + '/' + str(code.start_from_offset))
                            except:
                                pass
                            # Remove OF time information
                            shutil.rmtree(code.path + '/processor' + str(n_proc) + '/' + time_dir_mv + '/uniform') # Remove OF time information
                            # Rename timeDir
                            shutil.move(code.path + '/processor' + str(n_proc) + '/' + time_dir_mv, 
                                        code.path + '/processor' + str(n_proc) + '/' + str(code.start_from_offset)) # Workaround to create 1, 1001, 2001, ... as new starts for corr>1
    
                    time_dir = str(code.start_from_offset) # For later update of code.start_from
                    current_cpl_interval = code.cpl_interval-intermediate_steadyCfdResult_offset
                    # Update label
                    code.start_from_modified = True

            print(code.name + ': Latest time directory found: ' + time_dir)
    
            if tarball == True: # Archive time dirs to avoid too many folders
                print(sort)
                print(dir_list)
                dir_list = [x for x in dir_list if float(x) > 0]
                print(dir_list)
                if len(dir_list) > 10: # Keep newest 10 folders out of archive
                    # Sort ascending to ...
                    sort = np.argsort(np.array(dir_list).astype(float))
                    # ... get first time_dir
                    tar_dir = dir_list[sort[0]]
                    print(code.name + ': Archive time directory: ' + tar_dir)
                    cmd = 'pushd ' + code.path + ' > /dev/null && '
                    + '/bin/tar -rf timeDirs.tar ' + tar_dir + ' && rm -rf '
                    + tar_dir + ' && popd > /dev/null'
                    subprocess.check_call(cmd, shell=True)
    
            # The following vars can be used by code to start it at specified time
            logging.debug('PROTOTYPE: cplInterval, deltaT, writeInterval from '
                  'intputDeck.inp is overwritten.')
            # Write as string in convertformat as written by OpenFOAM
            code.start_from = time_dir
            if code.use_intermediate_steadyCfdResult_as_init == False:
                code.stopAt = float(code.start_from) + current_cpl_interval
            elif code.use_intermediate_steadyCfdResult_as_init == True:
                code.stopAt = float(code.start_from) + current_cpl_interval
                code.write_interval = current_cpl_interval

    
            # Modify control_dict: startTime, endTime, deltaT, writeInterval
            control_dict = code.path + '/system/controlDict'
            try:
                file = open(control_dict, 'r')
                strng = file.read()
                strng = re.sub(r'^startTime.*$', 'startTime       '
                               + code.start_from + ';', strng, flags=re.M)
                strng = re.sub(r'^endTime.*$', 'endTime         '
                               + str(code.stopAt) + ';', strng, flags=re.M)
                strng = re.sub(r'^deltaT.*$', 'deltaT          '
                               + str(code.delta_t) + ';', strng, flags=re.M)
                strng = re.sub(r'^writeInterval.*$', 'writeInterval   '
                               + str(code.write_interval) + ';', strng, flags=re.M)
                file.close()
                file = open(control_dict, 'w')
                file.write(strng)
                file.close()
            except:
                sys.exit('Error: Cannot open file for reading/writing: '
                         + control_dict)
    
    
        ###########################################################################
        ## CFX
        ###########################################################################
        elif code.name == 'CFX18':
            print('.')
            #code.cplInterval = 100   # Should be adjusted automatically
            #code.deltaT = 1          # Internal time step
            #code.stopAt =           # Not relevant for CFX
            #code.startFrom =        # Always latest?
            #code.writeInterval =    # Not relevant for CFX
        return None
