#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Module to import user run settings """

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def read_input(input_path, fea, cfd, cpl):
    """
    This function reads the run settings that are specified by the user
    and adds instance attributes.
    """

    print('Preprocessing: Read definitions and settings from '
          + str(input_path))
    import sys, os, configparser
    import numpy as np  # for np.nan fallback
    config = configparser.ConfigParser()

    # Check if input_path exists
    if os.path.exists(input_path):
        config.read(input_path)
    else:
        sys.exit('Error: ' + input_path + ' does not exist or cannot be read.')

    # Initialize cpl settings
    cpl.mode = config.get('cpl', 'mode')
    cpl.max_iter = int(config.get('cpl', 'maxIter'))
    cpl.simtime['maxSimTime'] = float(config.get('cpl', 'maxSimTime', fallback=9999999999))
    cpl.cpl_interval_dynamic = config.get('cpl', 'cplIntervalDynamic', fallback='static')
    cpl.cpl_interval_dynamic_acceleration_factor = float(config.get('cpl', 'cplIntervalDynamicAccelerationFactor', fallback=1.0))
    cpl.max_interface_temperature_change = float(config.get('cpl', 'maxInterfaceTemperatureChange', fallback=9999999999))
    cpl.max_cpl_interval = float(config.get('cpl', 'maxCplInterval', fallback=9999999999))
    cpl.backup = int(config.get('cpl', 'backup', fallback=1e6))
    cpl.interface_surface = config.get('cpl', 'interfaceSurface')
    cpl.interpolation_method = config.get('cpl', 'interpolationMethod',
                                          fallback='griddata')
    cpl.interpolation_method_function = config.get(
        'cpl', 'interpolationMethodFunction', fallback='linear')
    cpl.interpolation_method_smooth = float(config.get(
        'cpl', 'interpolationMethodSmooth', fallback=0))
    cpl.interp1d_axis = config.get('cpl', 'interp1dAxis', fallback='none')
    cpl.n_outer_correctors = int(config.get('cpl', 'nOuterCorrectors'))

    # Initialize fea settings
    fea.name = config.get('fea', 'codeName')
    fea.source_path = config.get('fea', 'sourcePath')
    fea.path = config.get('fea', 'path')
    fea.log = config.get('fea', 'log')
    fea.lockfile = config.get('fea', 'lockfile')
    fea.start_file = config.get('fea', 'startFile')
    fea.restart_file = config.get('fea', 'restartFile')
    fea.patch_name = config.get('fea', 'patchName')
    fea.init_value = float(config.get('fea', 'initValue'))
    fea.delta_t = float(config.get('fea', 'deltaT'))
    fea.cpl_interval = float(config.get('fea', 'cplInterval'))
    fea.result_format = config.get('fea', 'resultFormat')
    fea.n_procs = int(config.get('fea', 'nProcs'))
    fea.input_variable = config.get('fea', 'inputVariable')
    if fea.input_variable == 'Robin':
        fea.h_coeff = float(config.get('fea', 'hCoeff', fallback=np.nan)) # fallback=0 will yield an error when dividing
        fea.variable_h_coeff = config.getboolean('fea', 'variableHCoeff', fallback=False)
        fea.init_h_coeff = fea.h_coeff

    fea.n_iter_interval_change = int(config.get('fea',
                                                'noIterCplIntervalChange',
                                                fallback=9999999999))
    fea.transient_bc = config.getboolean('fea', 'transientBC', fallback=True)
    fea.restart = config.getboolean('fea', 'restart', fallback=False)
    fea.convergence_norm = config.get('fea', 'convergenceNorm'
                                      , fallback='np.inf')
    fea.convergence_limit = float(config.get('fea', 'convergenceLimit',
                                             fallback=1))

    # Initialize cfd settings
    cfd.name = config.get('cfd', 'codeName')
    # TODO: source_path is deprecated. Remove.
    cfd.source_path = config.get('cfd', 'sourcePath', fallback='deprecated')
    cfd.path = config.get('cfd', 'path')
    cfd.log = config.get('cfd', 'log')
    cfd.lockfile = config.get('cfd', 'lockfile')
    cfd.start_file = config.get('cfd', 'startFile')
    cfd.patch_name = config.get('cfd', 'patchName')
    cfd.init_value = float(config.get('cfd', 'initValue'))
    cfd.start_from = config.get('cfd', 'startFrom')
    cfd.delta_t = float(config.get('cfd', 'deltaT'))
    cfd.cpl_interval = float(config.get('cfd', 'cplInterval'))
    cfd.write_interval = float(config.get('cfd', 'writeInterval'))
    cfd.use_intermediate_steadyCfdResult_as_init = config.getboolean('cfd', 'reuse_steady_intermediate_result', fallback=False)
    cfd.n_procs = int(config.get('cfd', 'nProcs'))
    cfd.input_variable = config.get('cfd', 'inputVariable')
    if cfd.input_variable == 'Robin':
        cfd.h_coeff = float(config.get('cfd', 'hCoeff', fallback=np.nan))
        cfd.variable_h_coeff = config.getboolean('cfd', 'variableHCoeff', fallback=False)
        cfd.init_h_coeff = cfd.h_coeff
        
    cfd.restart = config.getboolean('cfd', 'restart', fallback=False)
    cfd.convergence_norm = config.get('cfd', 'convergenceNorm',
                                      fallback='np.inf')
    cfd.convergence_limit = float(config.get('cfd', 'convergenceLimit',
                                             fallback=1))
    cfd.relax_method = config.get('cfd', 'relaxMethod', fallback='none')
    cfd.relax_omega_init = float(config.get('cfd', 'relaxOmegaInit', fallback=1.0))

    # Return objects
    return None
