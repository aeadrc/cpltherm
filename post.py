#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" The following is executed only after the convergence check was True """

"""
This file is part of the coupling program cplTherm, see master.py.

It is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def post_run(code, cpl, tarball):
    """ Perform some post processing tasks on the fly (rename files etc.) """
    import os
    import subprocess
    import shutil
    import logging

    os.chdir(code.path) # Change to dir where commands have to be executed
    # File modification after convergence check
    if code.name == 'ccx':
        if cpl.n_iter == 1 and code.restart == False:
            # Save field results only at final corrector step for next time
            # step (for all previous corrector steps the converged results of
            # the old timestep are used)
            if cpl.final_corrector_step == True:
                shutil.move(code.path + '/' + code.start_file + '.rout',
                            code.path + '/' + code.restart_file + '.rin')

            # Further Log Modification
            if cpl.corrector_step == 1:
                shutil.copy2(code.path + '/' + code.start_file + '.dat',
                             code.path + '/' + code.log + '.dat')
                shutil.copy2(code.path + '/' + code.start_file + '.cvg',
                             code.path + '/' + code.log + '.cvg')
            elif cpl.corrector_step > 1:
                cmd = ('cat ' + code.path + '/' + code.start_file
                       + '.dat >> ' + code.path + '/' +  code.log + '.dat')
                print('TODO: Better to use python3 file manipulation: ' + cmd)
                subprocess.check_call(cmd, shell=True)
                cmd = ('tail -n +5 ' + code.path + '/' + code.start_file
                       + '.cvg >> ' + code.path + '/' +  code.log + '.cvg')
                print('TODO: Better to use python3 file manipulation: ' + cmd)
                subprocess.check_call(cmd, shell=True)

            # Only available if CalculiX Extras is available
            if code.result_format == 'exo':
                if not os.path.exists(code.path + '/results'):
                    # Create debug subdir that contains the interpolation files
                    os.mkdir(code.path + '/results')
                shutil.copy2(code.path + '/' + code.start_file + '.exo',
                             code.path + '/results/' + code.start_file + '_'
                             + str(cpl.n_iter) + '_' + str(cpl.corrector_step)
                             + '.exo')

        else:
            # Save field results only at final corrector step for next time
            # step (for all previous corrector steps the converged results of
            # the old timestep are used)
            if cpl.final_corrector_step == True:
                shutil.move(code.path + '/' + code.restart_file + '.rout',
                            code.path + '/' + code.restart_file + '.rin')

            # Save other information at all corrector steps
            if (code.restart == True and cpl.n_iter == 1
                and cpl.corrector_step == 1):
                shutil.copy2(code.path + '/' + code.restart_file + '.dat',
                             code.path + '/' + code.log + '.dat')
                shutil.copy2(code.path + '/' + code.restart_file + '.cvg',
                             code.path + '/' + code.log + '.cvg')
            else:
                cmd = ('cat ' + code.path + '/' + code.restart_file
                       + '.dat >> ' + code.path + '/' +  code.log + '.dat')
                print('TODO: Better to use python3 file manipulation: ' + cmd)
                subprocess.check_call(cmd, shell=True)
                cmd = ('tail -n +5 ' + code.path + '/' + code.restart_file
                       + '.cvg >> ' + code.path + '/' +  code.log + '.cvg')
                print('TODO: Better to use python3 file manipulation: ' + cmd)
                subprocess.check_call(cmd, shell=True)
            # Only available if CalculiX Extras is installed
            if code.result_format == 'exo':
                shutil.copy2(code.path + '/' + code.restart_file + '.exo',
                             code.path + '/results/' + code.restart_file + '_'
                             + str(cpl.n_iter) + '_' + str(cpl.corrector_step)
                             + '.exo')
                # Archive results files to avoid many small files
                # (disk performance)
                if tarball == True:
                    cmd = ('pushd ' + code.path + '/results > /dev/null && '
                           + 'find *.exo | /bin/tar -rf results.tar -T - && '
                           'rm -f *.exo && popd > /dev/null')
                    subprocess.check_call(cmd, shell=True)

    elif code.name in ['OF4', 'OF6']:
        pass

    elif code.name == 'CFX18':
        # Postprocessing: Export temperature/heatFlux/... probes
        # Execute only if exportLineVariable.cse is defined correctly.
        # Skip otherwise with warning..
        try:
            cmd = ('cfx5post -res ' + code.start_file
                   + '_001.res -batch exportLineVariables.cse')
            print(code.name + ': ' + cmd)
            subprocess.check_call(cmd, shell=True)
        except:
            # TODO: Distinguish exceptions (e.g. FileNotFoundError)
            logging.warning('Cannot execute ' + str(cmd)
                            + '. Check exportLineVariables.cse.')

        # Move / copy various files
        if cpl.final_corrector_step == True:
            # Rename results file only at final corrector iteration
            # Update for next iteration
            shutil.move(code.path + '/' + code.start_file + '_001.res',
                        code.path + '/' + code.start_file + '_latestTime.res')
        else:
            os.remove(code.path + '/' + code.start_file + '_001.res')
        # Further file modification
        if cpl.n_iter == 1:
            # Create results directory
            # Only at first corrector step of first cpl.n_iter
            if cpl.corrector_step == 1:
                if not os.path.exists(code.path + '/results'):
                    os.mkdir(code.path + '/results')
                else:
                    shutil.rmtree(code.path + '/results')
                    os.mkdir(code.path + '/results')

        # Move transient results of intermediate timesteps to result folder
        # if there are any
        if os.path.exists(code.path + '/' + code.start_file + '_001'):
            for file_name in os.listdir(code.path + '/' + code.start_file
                                        + '_001'):
                shutil.move(code.path + '/' + code.start_file + '_001/'
                            + file_name, code.path + '/results/' + file_name
                            + '.' + str(cpl.n_iter) + '.'
                            + str(cpl.corrector_step))
        # Move lineVariables.csv to result folder
        if os.path.exists(code.path + '/lineVariables.csv'):
            shutil.copy2(code.path + '/lineVariables.csv', code.path
                         + '/results/lineVariables.' + str(cpl.n_iter)
                         + '.' + str(cpl.corrector_step) + '.csv')
        shutil.move(code.path + '/' + code.start_file + '_001.out',
                    code.path + '/results/' + code.start_file + '.'
                    + str(cpl.n_iter) + '.' + str(cpl.corrector_step) + '.out')
        if os.path.exists(code.path + '/' + code.start_file + '_001'):
            os.rmdir(code.path + '/' + code.start_file + '_001')
            # Archive results files to avoid many small files(disk performance)
        if tarball == True:
            print('TODO: CHECK IF IMPLEMENTATION IS CORRECT!')
            cmd = ('pushd ' + code.path + '/results > /dev/null && '
                   + 'find . | /bin/tar -rf results.tar '
                   '--exclude="results.tar" -T - && popd > /dev/null')
            subprocess.check_call(cmd, shell=True)

    os.chdir(cpl.path) # Change back to coupling dir
    return None

